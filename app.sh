#!/bin/bash

if command -v java &>/dev/null; then
    echo "Java está instalado en la máquina."
    echo "Iniciando Aplicación"
    ./gradlew build
    ./gradlew assemble
     java -jar ./build/libs/library-1.0.jar
else
    echo "Java no está instalado en la máquina."
    echo "Terminando Proceso"
fi

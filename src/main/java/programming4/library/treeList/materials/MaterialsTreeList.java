package programming4.library.treeList.materials;

import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.list.materials.MaterialsList;
import programming4.library.tree.records.RecordsTree;
import programming4.library.treeList.materials.interfaces.IMaterialsTreeList;
import programming4.library.treeList.records.RecordsTreeList;

public class MaterialsTreeList extends RecordsTreeList<Material> implements IMaterialsTreeList {

  private final MaterialsList<Material> materialsList;

  public MaterialsTreeList() {
    super(new MaterialsList<>(), new RecordsTree<>());
    this.materialsList = (MaterialsList<Material>) this.getList();
  }

  @Override
  public MaterialsList<Material> filterByTitle(String title) {
    return materialsList.filterByTitle(title);
  }

  @Override
  public MaterialsList<Material> filterByAuthor(String author) {
    return materialsList.filterByAuthor(author);
  }

  @Override
  public MaterialsList<Material> filterByCategory(String category) {
    return materialsList.filterByCategory(category);
  }

  @Override
  public MaterialsList<Material> filterByIsbn(String isbn) {
    return materialsList.filterByIsbn(isbn);
  }

  @Override
  public MaterialsList<Material> filterByPublicationYear(int publicationYear) {
    return materialsList.filterByPublicationYear(publicationYear);
  }

  @Override
  public MaterialsList<Material> filterByPageCount(int pageCount) {
    return materialsList.filterByPageCount(pageCount);
  }

  @Override
  public MaterialsList<Material> filterByAvailable(boolean available) {
    return materialsList.filterByAvailable(available);
  }

  @Override
  public MaterialsList<Material> filterByQuantity(int quantity) {
    return materialsList.filterByQuantity(quantity);
  }

  @Override
  public MaterialsList<Material> filterByTypeMaterial(String typeMaterial) {
    return materialsList.filterByTypeMaterial(typeMaterial);
  }

  @Override
  public MaterialsList<Material> filterByLowStock() {
    return materialsList.filterByLowStock();
  }

  @Override
  public MaterialsList<Book> filterByEditorial(String editorial) {
    return materialsList.filterByEditorial(editorial);
  }

  @Override
  public MaterialsList<Book> filterOnlyBooks() {
    return materialsList.filterOnlyBooks();
  }

  @Override
  public MaterialsList<Magazine> filterOnlyMagazines() {
    return materialsList.filterOnlyMagazines();
  }
}

package programming4.library.treeList.materials.interfaces;

import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.list.materials.MaterialsList;

public interface IMaterialsTreeList {

  MaterialsList<Material> filterByTitle(String title);

  MaterialsList<Material> filterByAuthor(String author);

  MaterialsList<Material> filterByCategory(String category);

  MaterialsList<Material> filterByIsbn(String isbn);

  MaterialsList<Material> filterByPublicationYear(int publicationYear);

  MaterialsList<Material> filterByPageCount(int pageCount);

  MaterialsList<Material> filterByAvailable(boolean available);

  MaterialsList<Material> filterByQuantity(int quantity);

  MaterialsList<Material> filterByTypeMaterial(String typeMaterial);

  MaterialsList<Material> filterByLowStock();

  MaterialsList<Book> filterByEditorial(String editorial);

  MaterialsList<Book> filterOnlyBooks();

  MaterialsList<Magazine> filterOnlyMagazines();
}

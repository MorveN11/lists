package programming4.library.treeList.abstractions;

import programming4.library.list.abstractions.List;
import programming4.library.tree.abstractions.Tree;
import programming4.library.treeList.abstractions.interfaces.ITreeList;

public class TreeList<T extends Comparable<T>> implements ITreeList<T> {

  private final List<T> list;

  private final Tree<T> tree;

  public TreeList(List<T> list, Tree<T> tree) {
    this.list = list;
    this.tree = tree;
  }

  public TreeList() {
    this(new List<>(), new Tree<>());
  }

  public List<T> getList() {
    return list;
  }

  public Tree<T> getTree() {
    return tree;
  }

  @Override
  public T add(T value) {
    list.add(value);
    return tree.add(value);
  }

  @Override
  public T getByList(T value) {
    return list.get(value);
  }

  @Override
  public T getByTree(T value) {
    return tree.get(value);
  }

  @Override
  public T remove(T value) {
    list.remove(value);
    return tree.remove(value);
  }

  @Override
  public T update(T value, T updateValue) {
    list.update(value, updateValue);
    return tree.update(value, updateValue);
  }

  @Override
  public String toString() {
    return list.toString();
  }
}

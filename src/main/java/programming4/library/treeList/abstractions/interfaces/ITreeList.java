package programming4.library.treeList.abstractions.interfaces;

public interface ITreeList<T> {

  T add(T value);

  T getByList(T value);

  T getByTree(T value);

  T remove(T value);

  T update(T value, T updateValue);
}

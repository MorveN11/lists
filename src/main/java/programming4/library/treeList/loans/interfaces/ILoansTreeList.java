package programming4.library.treeList.loans.interfaces;

import java.time.LocalDate;
import programming4.library.list.loans.LoansList;

public interface ILoansTreeList {

  LoansList filterByBookId(String bookId);

  LoansList filterByPersonId(String personId);

  LoansList filterByLoanDate(LocalDate loanDate);

  LoansList filterByReturnDate(LocalDate returnDate);
}

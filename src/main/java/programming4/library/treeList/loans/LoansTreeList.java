package programming4.library.treeList.loans;

import java.time.LocalDate;
import programming4.library.classes.loans.Loan;
import programming4.library.list.loans.LoansList;
import programming4.library.tree.records.RecordsTree;
import programming4.library.treeList.loans.interfaces.ILoansTreeList;
import programming4.library.treeList.records.RecordsTreeList;

public class LoansTreeList extends RecordsTreeList<Loan> implements ILoansTreeList {

  private final LoansList loansList;

  public LoansTreeList() {
    super(new LoansList(), new RecordsTree<>());
    this.loansList = (LoansList) this.getList();
  }

  @Override
  public LoansList filterByBookId(String bookId) {
    return this.loansList.filterByBookId(bookId);
  }

  @Override
  public LoansList filterByPersonId(String personId) {
    return this.loansList.filterByPersonId(personId);
  }

  @Override
  public LoansList filterByLoanDate(LocalDate loanDate) {
    return this.loansList.filterByLoanDate(loanDate);
  }

  @Override
  public LoansList filterByReturnDate(LocalDate returnDate) {
    return this.loansList.filterByReturnDate(returnDate);
  }
}

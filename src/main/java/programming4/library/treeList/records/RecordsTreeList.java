package programming4.library.treeList.records;

import java.util.UUID;
import programming4.library.classes.interfaces.Record;
import programming4.library.list.records.RecordsList;
import programming4.library.tree.records.RecordsTree;
import programming4.library.treeList.abstractions.TreeList;
import programming4.library.treeList.records.interfaces.IRecordsTreeList;

public class RecordsTreeList<T extends Record & Comparable<T>> extends TreeList<T> implements IRecordsTreeList<T> {

  private final RecordsList<T> recordsList;

  private final RecordsTree<T> recordsTree;

  public RecordsTreeList(RecordsList<T> recordsList, RecordsTree<T> recordsTree) {
    super(recordsList, recordsTree);
    this.recordsList = recordsList;
    this.recordsTree = recordsTree;
  }

  public RecordsTreeList() {
    this(new RecordsList<>(), new RecordsTree<>());
  }

  @Override
  public T removeById(String id) {
    recordsList.removeById(id);
    return recordsTree.removeById(id);
  }

  @Override
  public T getByIdByList(String id) {
    return recordsList.getById(id);
  }

  @Override
  public T getByIdByTree(String id) {
    return recordsTree.getById(id);
  }

  @Override
  public T updateById(String id, T valueUpdated) {
    recordsList.updateById(id, valueUpdated);
    return recordsTree.updateById(id, valueUpdated);
  }

  @Override
  public T removeById(UUID id) {
    recordsList.removeById(id);
    return recordsTree.removeById(id);
  }

  @Override
  public T getByIdByList(UUID id) {
    return recordsList.getById(id);
  }

  @Override
  public T getByIdByTree(UUID id) {
    return recordsTree.getById(id);
  }

  @Override
  public T updateById(UUID id, T valueUpdated) {
    recordsList.updateById(id, valueUpdated);
    return recordsTree.updateById(id, valueUpdated);
  }
}

package programming4.library.treeList.records.interfaces;

import java.util.UUID;

public interface IRecordsTreeList<T> {

  T removeById(String id);

  T getByIdByList(String id);

  T getByIdByTree(String id);

  T updateById(String id, T valueUpdated);

  T removeById(UUID id);

  T getByIdByList(UUID id);

  T getByIdByTree(UUID id);

  T updateById(UUID id, T valueUpdated);
}

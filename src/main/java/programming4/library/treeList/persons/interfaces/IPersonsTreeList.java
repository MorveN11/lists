package programming4.library.treeList.persons.interfaces;

import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.list.persons.PersonsList;

public interface IPersonsTreeList {

  PersonsList<Person> filterByName(String name);

  PersonsList<Person> filterByLastname(String lastname);

  PersonsList<Person> filterByAddress(String address);

  PersonsList<Person> filterByEmail(String email);

  PersonsList<Person> filterByPhoneNumber(int phoneNumber);

  PersonsList<Person> filterByNumBooks(int numBooks);

  PersonsList<Person> filterByDebt(double debt);

  PersonsList<Student> filterOnlyStudents();

  PersonsList<Teacher> filterOnlyTeachers();
}

package programming4.library.treeList.persons;

import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.list.persons.PersonsList;
import programming4.library.tree.records.RecordsTree;
import programming4.library.treeList.persons.interfaces.IPersonsTreeList;
import programming4.library.treeList.records.RecordsTreeList;

public class PersonsTreeList extends RecordsTreeList<Person> implements IPersonsTreeList {

  private final PersonsList<Person> personsList;

  public PersonsTreeList() {
    super(new PersonsList<>(), new RecordsTree<>());
    this.personsList = (PersonsList<Person>) getList();
  }

  @Override
  public PersonsList<Person> filterByName(String name) {
    return personsList.filterByName(name);
  }

  @Override
  public PersonsList<Person> filterByLastname(String lastname) {
    return personsList.filterByLastname(lastname);
  }

  @Override
  public PersonsList<Person> filterByAddress(String address) {
    return personsList.filterByAddress(address);
  }

  @Override
  public PersonsList<Person> filterByEmail(String email) {
    return personsList.filterByEmail(email);
  }

  @Override
  public PersonsList<Person> filterByPhoneNumber(int phoneNumber) {
    return personsList.filterByPhoneNumber(phoneNumber);
  }

  @Override
  public PersonsList<Person> filterByNumBooks(int numBooks) {
    return personsList.filterByNumBooks(numBooks);
  }

  @Override
  public PersonsList<Person> filterByDebt(double debt) {
    return personsList.filterByDebt(debt);
  }

  @Override
  public PersonsList<Student> filterOnlyStudents() {
    return personsList.filterOnlyStudents();
  }

  @Override
  public PersonsList<Teacher> filterOnlyTeachers() {
    return personsList.filterOnlyTeachers();
  }
}

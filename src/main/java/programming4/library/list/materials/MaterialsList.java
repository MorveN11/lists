package programming4.library.list.materials;

import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.list.abstractions.Node;
import programming4.library.list.materials.interfaces.IMaterialsList;
import programming4.library.list.records.RecordsList;
import programming4.library.utils.Constants;
import programming4.library.utils.ValidateInputs;

public class MaterialsList<T extends Material> extends RecordsList<T> implements IMaterialsList<T> {

  @Override
  public MaterialsList<T> filterByTitle(String title) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (ValidateInputs.similarString(current.getValue().getTitle(), title)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByAuthor(String author) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (ValidateInputs.similarString(current.getValue().getAuthor(), author)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByCategory(String category) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue().getCategory().equals(category)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByIsbn(String isbn) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue().getIsbn().equals(isbn)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByPublicationYear(int publicationYear) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue().getPublicationYear() == publicationYear) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByPageCount(int pageCount) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue().getPageCount() == pageCount) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByAvailable(boolean available) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue().isAvailable() == available) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByQuantity(int quantity) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue().getQuantity() == quantity) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByTypeMaterial(String typeMaterial) {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (ValidateInputs.similarString(current.getValue().getTypeMaterial(), typeMaterial)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<T> filterByLowStock() {
    Node<T> current = getHeadNode();
    MaterialsList<T> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue().getQuantity() <= Constants.LOW_STOCK) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<Book> filterByEditorial(String editorial) {
    Node<T> current = getHeadNode();
    MaterialsList<Book> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue() instanceof Book &&
              ValidateInputs.similarString(((Book) current.getValue()).getEditorial(), editorial)) {
        filterData.add((Book) current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<Book> filterOnlyBooks() {
    Node<T> current = getHeadNode();
    MaterialsList<Book> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue() instanceof Book) {
        filterData.add((Book) current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public MaterialsList<Magazine> filterOnlyMagazines() {
    Node<T> current = getHeadNode();
    MaterialsList<Magazine> filterData = new MaterialsList<>();
    while (current != null) {
      if (current.getValue() instanceof Magazine) {
        filterData.add((Magazine) current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }
}

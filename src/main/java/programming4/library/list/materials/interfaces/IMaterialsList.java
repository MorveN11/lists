package programming4.library.list.materials.interfaces;

import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.list.materials.MaterialsList;

public interface IMaterialsList<T extends Material> {

  MaterialsList<T> filterByTitle(String title);

  MaterialsList<T> filterByAuthor(String author);

  MaterialsList<T> filterByCategory(String category);

  MaterialsList<T> filterByIsbn(String isbn);

  MaterialsList<T> filterByPublicationYear(int publicationYear);

  MaterialsList<T> filterByPageCount(int pageCount);

  MaterialsList<T> filterByAvailable(boolean available);

  MaterialsList<T> filterByQuantity(int quantity);

  MaterialsList<T> filterByTypeMaterial(String typeMaterial);

  MaterialsList<T> filterByLowStock();

  MaterialsList<Book> filterByEditorial(String editorial);

  MaterialsList<Book> filterOnlyBooks();

  MaterialsList<Magazine> filterOnlyMagazines();
}

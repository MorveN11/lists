package programming4.library.list.abstractions;

public class Node<T> {

  private T value;

  private Node<T> next;

  public Node(T value) {
    this.value = value;
    this.next = null;
  }

  public T getValue() {
    return value;
  }

  public void setValue(T value) {
    this.value = value;
  }

  public Node<T> getNext() {
    return next;
  }

  public void setNext(Node<T> next) {
    this.next = next;
  }

  @Override
  public String toString() {
    return this.value.toString() + " -> " + this.next.getValue().toString();
  }

  @Override
  public boolean equals(Object object) {
    if (object == null) {
      return false;
    }
    if (this.getClass() != object.getClass()) {
      return false;
    }
    Node<?> obj = (Node<?>) object;
    return this.value.equals(obj.getValue()) && this.next.getValue().equals(obj.getNext().getValue());
  }
}

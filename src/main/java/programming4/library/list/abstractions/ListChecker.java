package programming4.library.list.abstractions;

import programming4.library.list.abstractions.interfaces.IListChecker;

public class ListChecker<T> implements IListChecker<T> {

  private final List<T> list;

  public ListChecker(List<T> list) {
    this.list = list;
  }

  private boolean contains(T value, Node<T> nextValue) {
    if (value == nextValue.getValue()) {
      return true;
    } else if (nextValue.getNext() != null) {
      return contains(value, nextValue.getNext());
    }
    return false;
  }

  @Override
  public boolean contains(T value) {
    if (list.getHeadNode().getValue() == value) {
      return true;
    } else if (list.getHeadNode().getNext() != null) {
      return contains(value, list.getHeadNode().getNext());
    }
    return false;
  }

  @Override
  public int size() {
    int i = 0;
    Node<T> current = list.getHeadNode();
    while (current != null) {
      i++;
      current = current.getNext();
    }
    return i;
  }

  @Override
  public boolean isEmpty() {
    return list.getHeadNode() == null;
  }

  @Override
  public String toString() {
    return list.toString();
  }

  @Override
  public boolean equals(Object object) {
    if (object == null) {
      return false;
    }
    if (this.getClass() != object.getClass()) {
      return false;
    }
    ListChecker<?> obj = (ListChecker<?>) object;
    return this.list.equals(obj.list);
  }
}

package programming4.library.list.abstractions;

import programming4.library.errors.NotFoundException;
import programming4.library.list.abstractions.interfaces.IList;

public class List<T> implements IList<T> {

  private final ListChecker<T> checker;

  private Node<T> head;

  public List() {
    this.head = null;
    this.checker = new ListChecker<>(this);
  }

  protected Node<T> getHeadNode() {
    return head;
  }

  protected void setHeadNode(Node<T> head) {
    this.head = head;
  }

  protected boolean invalidIndex(int index) {
    return index < 0 || index >= size();
  }

  @Override
  public boolean contains(T value) {
    return checker.contains(value);
  }

  @Override
  public int size() {
    return checker.size();
  }

  @Override
  public boolean isEmpty() {
    return checker.isEmpty();
  }

  @Override
  public T add(T value) {
    if (value == null) {
      return null;
    }
    if (head == null) {
      head = new Node<>(value);
      return head.getValue();
    }
    Node<T> current = head;
    while (current.getNext() != null) {
      current = current.getNext();
    }
    Node<T> newNode = new Node<>(value);
    current.setNext(newNode);
    return newNode.getValue();
  }

  @Override
  public T addAtIndex(int index, T value) {
    if (invalidIndex(index)) {
      throw new IndexOutOfBoundsException();
    }
    if (index == 0) {
      Node<T> newNode = new Node<>(value);
      newNode.setNext(head.getNext());
      head = newNode;
      return newNode.getValue();
    }
    Node<T> newNode = new Node<>(value);
    Node<T> current = head;
    for (int i = 0; i < index - 1; i++) {
      current = current.getNext();
    }
    newNode.setNext(current.getNext());
    current.setNext(newNode);
    return newNode.getValue();
  }

  @Override
  public boolean addAll(IList<T> list) {
    if (list == null) {
      return false;
    }
    for (int i = 0; i < list.size(); i++) {
      add(list.getByIndex(i));
    }
    return true;
  }

  @Override
  public T getByIndex(int index) {
    if (invalidIndex(index)) {
      throw new IndexOutOfBoundsException();
    }
    Node<T> current = head;
    for (int i = 0; i < index; i++) {
      current = current.getNext();
    }
    return current.getValue();
  }

  @Override
  public T updateByIndex(int index, T value) {
    if (invalidIndex(index)) {
      throw new IndexOutOfBoundsException();
    }
    Node<T> current = head;
    for (int i = 0; i < index; i++) {
      current = current.getNext();
    }
    current.setValue(value);
    return current.getValue();
  }

  @Override
  public T removeAtIndex(int index) {
    if (invalidIndex(index)) {
      throw new IndexOutOfBoundsException();
    }
    if (index == 0) {
      if (head == null) {
        return null;
      }
      T current = head.getValue();
      head = head.getNext();
      return current;
    }
    Node<T> current = head;
    for (int i = 0; i < index - 1; i++) {
      current = current.getNext();
    }
    Node<T> removedNode = current.getNext();
    current.setNext(removedNode.getNext());
    return removedNode.getValue();
  }

  @Override
  public T get(T value) {
    Node<T> current = head;
    while (current != null) {
      if (current.getValue().equals(value)) {
        return value;
      }
      current = current.getNext();
    }
    throw new NotFoundException();
  }

  @Override
  public T update(T value, T updateValue) {
    Node<T> current = head;
    while (current != null) {
      if (current.getValue().equals(value)) {
        current.setValue(updateValue);
        return updateValue;
      }
      current = current.getNext();
    }
    throw new NotFoundException();
  }

  @Override
  public T remove(T value) {
    if (head == null) {
      throw new NotFoundException();
    }
    if (head.getValue().equals(value)) {
      head = head.getNext();
      return value;
    }
    Node<T> previous = head;
    Node<T> current = head.getNext();
    while (current != null) {
      if (current.getValue().equals(value)) {
        previous.setNext(current.getNext());
        return value;
      }
      current = current.getNext();
    }
    throw new NotFoundException();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    Node<T> current = head;
    while (current != null) {
      sb.append(current.getValue());
      sb.append("\n");
      current = current.getNext();
    }
    return sb.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof List<?> other)) {
      return false;
    }
    if (size() != other.size()) {
      return false;
    }
    Node<T> current = head;
    Node<?> otherCurrent = other.getHeadNode();
    while (current != null) {
      if (!current.getValue().equals(otherCurrent.getValue())) {
        return false;
      }
      current = current.getNext();
      otherCurrent = otherCurrent.getNext();
    }
    return true;
  }
}

package programming4.library.list.abstractions.interfaces;

public interface IListChecker<T> {

  boolean contains(T value);

  int size();

  boolean isEmpty();
}

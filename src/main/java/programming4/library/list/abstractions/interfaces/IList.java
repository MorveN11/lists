package programming4.library.list.abstractions.interfaces;

public interface IList<T> {

  boolean contains(T value);

  int size();

  boolean isEmpty();

  T add(T value);

  T addAtIndex(int index, T value);

  boolean addAll(IList<T> list);

  T getByIndex(int index);

  T updateByIndex(int index, T value);

  T removeAtIndex(int index);

  T get(T value);

  T update(T value, T updateValue);

  T remove(T value);
}

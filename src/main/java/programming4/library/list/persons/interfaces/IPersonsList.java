package programming4.library.list.persons.interfaces;

import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.list.persons.PersonsList;

public interface IPersonsList<T extends Person> {

  PersonsList<T> filterByName(String name);

  PersonsList<T> filterByLastname(String lastname);

  PersonsList<T> filterByAddress(String address);

  PersonsList<T> filterByEmail(String email);

  PersonsList<T> filterByPhoneNumber(int phoneNumber);

  PersonsList<T> filterByNumBooks(int numBooks);

  PersonsList<T> filterByDebt(double debt);

  PersonsList<Student> filterOnlyStudents();

  PersonsList<Teacher> filterOnlyTeachers();
}

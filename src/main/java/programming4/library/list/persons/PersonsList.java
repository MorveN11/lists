package programming4.library.list.persons;

import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.list.abstractions.Node;
import programming4.library.list.persons.interfaces.IPersonsList;
import programming4.library.list.records.RecordsList;
import programming4.library.utils.ValidateInputs;

public class PersonsList<T extends Person> extends RecordsList<T> implements IPersonsList<T> {

  public PersonsList() {
    super();
  }

  @Override
  public PersonsList<T> filterByName(String name) {
    Node<T> current = getHeadNode();
    PersonsList<T> filterData = new PersonsList<>();
    while (current != null) {
      if (ValidateInputs.similarString(current.getValue().getName(), name)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public PersonsList<T> filterByLastname(String lastname) {
    Node<T> current = getHeadNode();
    PersonsList<T> filterData = new PersonsList<>();
    while (current != null) {
      if (ValidateInputs.similarString(current.getValue().getLastname(), lastname)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public PersonsList<T> filterByAddress(String address) {
    Node<T> current = getHeadNode();
    PersonsList<T> filterData = new PersonsList<>();
    while (current != null) {
      if (ValidateInputs.similarString(current.getValue().getAddress(), address)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public PersonsList<T> filterByEmail(String email) {
    Node<T> current = getHeadNode();
    PersonsList<T> filterData = new PersonsList<>();
    while (current != null) {
      if (ValidateInputs.similarString(current.getValue().getEmail(), email)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public PersonsList<T> filterByPhoneNumber(int phoneNumber) {
    Node<T> current = getHeadNode();
    PersonsList<T> filterData = new PersonsList<>();
    while (current != null) {
      if (current.getValue().getPhoneNumber() == phoneNumber) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public PersonsList<T> filterByNumBooks(int numBooks) {
    Node<T> current = getHeadNode();
    PersonsList<T> filterData = new PersonsList<>();
    while (current != null) {
      if (current.getValue().getNumBooks() == numBooks) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public PersonsList<T> filterByDebt(double debt) {
    Node<T> current = getHeadNode();
    PersonsList<T> filterData = new PersonsList<>();
    while (current != null) {
      if (current.getValue().getDebt() == debt) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public PersonsList<Student> filterOnlyStudents() {
    Node<T> current = getHeadNode();
    PersonsList<Student> filterData = new PersonsList<>();
    while (current != null) {
      if (current.getValue() instanceof Student) {
        filterData.add((Student) current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public PersonsList<Teacher> filterOnlyTeachers() {
    Node<T> current = getHeadNode();
    PersonsList<Teacher> filterData = new PersonsList<>();
    while (current != null) {
      if (current.getValue() instanceof Teacher) {
        filterData.add((Teacher) current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }
}

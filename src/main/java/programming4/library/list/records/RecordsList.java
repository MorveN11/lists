package programming4.library.list.records;

import java.util.UUID;
import programming4.library.classes.interfaces.Record;
import programming4.library.errors.NotFoundException;
import programming4.library.errors.NotUniqueIdException;
import programming4.library.list.abstractions.List;
import programming4.library.list.abstractions.Node;
import programming4.library.list.records.interfaces.IRecordsList;

public class RecordsList<T extends Record> extends List<T> implements IRecordsList<T> {

  public RecordsList() {
    super();
  }

  private boolean repeatedId(T value) {
    Node<T> current = getHeadNode();
    while (current != null) {
      if (current.getValue().getId().equals(value.getId())) {
        return true;
      }
      current = current.getNext();
    }
    return false;
  }

  @Override
  public T add(T value) {
    if (repeatedId(value)) {
      throw new NotUniqueIdException();
    }
    return super.add(value);
  }

  @Override
  public T addAtIndex(int index, T value) {
    if (repeatedId(value)) {
      throw new NotUniqueIdException();
    }
    return super.addAtIndex(index, value);
  }

  @Override
  public T getById(String id) {
    Node<T> current = getHeadNode();
    while (current != null) {
      if (current.getValue().getId().toString().equals(id)) {
        return current.getValue();
      }
      current = current.getNext();
    }
    throw new NotFoundException();
  }

  @Override
  public T getById(UUID id) {
    return getById(id.toString());
  }

  @Override
  public boolean updateById(String id, T value) {
    Node<T> current = getHeadNode();
    while (current != null) {
      if (current.getValue().getId().toString().equals(id)) {
        current.setValue(value);
        return true;
      }
      current = current.getNext();
    }
    throw new NotFoundException();
  }

  @Override
  public boolean updateById(UUID id, T value) {
    return updateById(id.toString(), value);
  }

  @Override
  public boolean removeById(String id) {
    Node<T> current = getHeadNode();
    if (current.getValue().getId().toString().equals(id)) {
      Node<T> tmpNode = current.getNext();
      setHeadNode(tmpNode);
      return true;
    }
    while (current != null && current.getNext() != null) {
      if (current.getNext().getValue().getId().toString().equals(id)) {
        Node<T> tmpNode = current.getNext();
        current.setNext(tmpNode.getNext());
        return true;
      }
      current = current.getNext();
    }
    throw new NotFoundException();
  }

  @Override
  public boolean removeById(UUID id) {
    return removeById(id.toString());
  }
}

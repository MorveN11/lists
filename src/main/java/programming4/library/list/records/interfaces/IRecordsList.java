package programming4.library.list.records.interfaces;

import java.util.UUID;

public interface IRecordsList<T> {

  T getById(String id);

  T getById(UUID id);

  boolean updateById(String id, T value);

  boolean updateById(UUID id, T value);

  boolean removeById(String id);

  boolean removeById(UUID id);
}

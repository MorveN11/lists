package programming4.library.list.loans;

import java.time.LocalDate;
import programming4.library.classes.loans.Loan;
import programming4.library.list.abstractions.Node;
import programming4.library.list.loans.interfaces.ILoansList;
import programming4.library.list.records.RecordsList;

public class LoansList extends RecordsList<Loan> implements ILoansList {

  public LoansList() {
    super();
  }

  @Override
  public LoansList filterByBookId(String bookId) {
    Node<Loan> current = getHeadNode();
    LoansList filterData = new LoansList();
    while (current != null) {
      if (current.getValue().getMaterial().getId().toString().equals(bookId)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public LoansList filterByPersonId(String personId) {
    Node<Loan> current = getHeadNode();
    LoansList filterData = new LoansList();
    while (current != null) {
      if (current.getValue().getPerson().getId().toString().equals(personId)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public LoansList filterByLoanDate(LocalDate loanDate) {
    Node<Loan> current = getHeadNode();
    LoansList filterData = new LoansList();
    while (current != null) {
      if (current.getValue().getLoanDate().equals(loanDate)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }

  @Override
  public LoansList filterByReturnDate(LocalDate returnDate) {
    Node<Loan> current = getHeadNode();
    LoansList filterData = new LoansList();
    while (current != null) {
      if (current.getValue().getReturnDate().equals(returnDate)) {
        filterData.add(current.getValue());
      }
      current = current.getNext();
    }
    return filterData;
  }
}

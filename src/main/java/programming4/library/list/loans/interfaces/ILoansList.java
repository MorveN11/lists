package programming4.library.list.loans.interfaces;

import java.time.LocalDate;
import programming4.library.list.loans.LoansList;

public interface ILoansList {

  LoansList filterByBookId(String bookId);

  LoansList filterByPersonId(String personId);

  LoansList filterByLoanDate(LocalDate loanDate);

  LoansList filterByReturnDate(LocalDate returnDate);
}

package programming4.library.utils;

import static programming4.library.utils.Constants.EMAIL_REGEX;
import static programming4.library.utils.Constants.ISBN_10_REGEX;
import static programming4.library.utils.Constants.ISBN_13_REGEX;
import static programming4.library.utils.Constants.PHONE_NUMBER_REGEX;
import static programming4.library.utils.Constants.STRING_REGEX;
import static programming4.library.utils.Constants.YEAR_REGEX;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class ValidateInputs {

  public static boolean validateInt(String input) {
    return input.matches("-?\\d+");
  }

  public static boolean validateDouble(String input) {
    return input.matches("-?\\d+(\\.\\d+)?");
  }

  public static boolean validateNotEmpty(String input) {
    return !input.isEmpty();
  }

  public static boolean validateString(String input) {
    return input.matches(STRING_REGEX)
            && validateNotEmpty(input);
  }

  public static boolean validateUuid(String input) {
    return input.matches(Constants.UUID_REGEX) && validateNotEmpty(input);
  }

  public static boolean validateEmail(String email) {
    return email.matches(EMAIL_REGEX) && validateNotEmpty(email);
  }

  public static boolean validateBoolean(String input) {
    return input.equalsIgnoreCase("true") || input.equalsIgnoreCase("false");
  }

  public static boolean validateISBN(String isbn) {
    String isbnWithoutDashes = isbn.replaceAll("-", "");
    return (isbnWithoutDashes.length() == 10 && Pattern.matches(ISBN_10_REGEX, isbnWithoutDashes)) ||
            (isbnWithoutDashes.length() == 13 && Pattern.matches(ISBN_13_REGEX, isbnWithoutDashes));
  }

  public static boolean validateYear(String year) {
    return Pattern.matches(YEAR_REGEX, year);
  }

  public static boolean similarString(String str1, String str2) {
    Set<Character> set1 = new HashSet<>();
    Set<Character> set2 = new HashSet<>();
    str1 = str1.trim().toLowerCase();
    str2 = str2.trim().toLowerCase();
    for (char c : str1.toCharArray()) {
      set1.add(c);
    }
    for (char c : str2.toCharArray()) {
      set2.add(c);
    }
    Set<Character> union = new HashSet<>(set1);
    union.addAll(set2);
    int intersectionSize = set1.size() + set2.size() - union.size();
    double similarity = (double) intersectionSize / union.size();
    return similarity >= Constants.PROXIMITY;
  }

  public static boolean validatePhoneNumber(String phoneNumber) {
    return phoneNumber.matches(PHONE_NUMBER_REGEX);
  }
}

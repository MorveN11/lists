package programming4.library.utils;

public class Constants {

  public static final int DEFAULT_DEBT = 0;

  public static final int DEFAULT_NUM_BOOKS = 0;

  public static final int DEFAULT_NUM_LOAN_MONTHS = 1;

  public static final int LOW_STOCK = 5;

  public static final String EMAIL_REGEX =
          "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";

  public static final String STRING_REGEX =
          "[a-zA-Z0-9 !@#$%^&*(),.?\":{}|<>\\\\\\\\-_+=;/'`~]+";

  public static final String UUID_REGEX =
          "[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}";

  protected static final String LINE = "--------------------------";

  protected static final String HALF_LINE = LINE.substring(0, LINE.length() / 2);

  protected static final String INVALID_INPUT = "Invalid input. Please try again.";

  protected static final String CANCEL_INPUT = "Write 'cancel' to cancel.";

  protected static final String CANCELED_INPUT = "Operation canceled.";

  protected static final String ISBN_10_REGEX =
          "^\\d{9}[0-9Xx]$|^(?:\\d{1,5}-){3}\\d{1,7}-[0-9Xx]$";

  protected static final String ISBN_13_REGEX =
          "^\\d{13}$|^(?:\\d{1,5}-){3}\\d{1,7}-\\d{1,2}$";

  protected static final String YEAR_REGEX = "^(19[0-9]{2}|20[0-9]{2})$";

  protected static final double PROXIMITY = 0.6;

  protected static final String PHONE_NUMBER_REGEX = "^\\d{8}$";
}

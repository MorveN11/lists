package programming4.library.errors;

public class NotImplementedFunctionException extends RuntimeException {

  public NotImplementedFunctionException() {
    super("This function is not implemented yet.");
  }
}

package programming4.library.errors;

public class NotValidIdException extends RuntimeException {

  public NotValidIdException() {
    super("The id inserted is not Valid");
  }
}

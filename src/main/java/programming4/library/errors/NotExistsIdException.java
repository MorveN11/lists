package programming4.library.errors;

public class NotExistsIdException extends RuntimeException {

  public NotExistsIdException() {
    super("Not exists id");
  }
}

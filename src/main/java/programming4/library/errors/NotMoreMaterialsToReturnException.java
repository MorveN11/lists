package programming4.library.errors;

public class NotMoreMaterialsToReturnException extends RuntimeException {

  public NotMoreMaterialsToReturnException() {
    super("You don't have more materials to return");
  }
}

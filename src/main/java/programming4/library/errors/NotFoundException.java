package programming4.library.errors;

public class NotFoundException extends RuntimeException {

  public NotFoundException() {
    super("Record not found");
  }
}

package programming4.library.errors;

public class NotUniqueIdException extends RuntimeException {

  public NotUniqueIdException() {
    super("The inserted Id is not unique");
  }
}

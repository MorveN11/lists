package programming4.library.classes.interfaces;

import java.util.UUID;
import programming4.library.utils.Constants;

public interface Record {

  UUID getId();

  default boolean invalidId(String id) {
    return !(id.matches(Constants.UUID_REGEX));
  }
}

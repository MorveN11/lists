package programming4.library.classes.persons;

import java.util.UUID;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.classes.persons.interfaces.ITeacher;
import programming4.library.utils.Ui;

public class Teacher extends Person implements ITeacher {

  public Teacher(String name, String lastname, String address, String email, int phoneNumber) {
    super(name, lastname, address, email, phoneNumber);
  }

  public Teacher(UUID id, String name, String lastname, String address, String email, int phoneNumber) {
    super(id, name, lastname, address, email, phoneNumber);
  }

  public Teacher(String id, String name, String lastname, String address, String email, int phoneNumber) {
    super(id, name, lastname, address, email, phoneNumber);
  }

  @Override
  public String toString() {
    return """
            %s
            %s
            %s
            """.formatted(Ui.printTitle("Teacher"), super.toString(), Ui.printLine());
  }
}

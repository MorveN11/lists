package programming4.library.classes.persons.abstractions.interfaces;

public interface IPerson {

  void takeMaterial();

  void returnMaterial();
}

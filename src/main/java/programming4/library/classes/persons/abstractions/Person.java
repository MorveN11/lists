package programming4.library.classes.persons.abstractions;

import static programming4.library.utils.Constants.DEFAULT_DEBT;
import static programming4.library.utils.Constants.DEFAULT_NUM_BOOKS;

import java.util.UUID;
import programming4.library.classes.interfaces.Record;
import programming4.library.classes.persons.abstractions.interfaces.IPerson;
import programming4.library.errors.NotMoreMaterialsToReturnException;
import programming4.library.errors.NotUniqueIdException;

public class Person implements Record, IPerson, Comparable<Person> {

  private final UUID uuid;

  private String name;

  private String lastname;

  private String address;

  private String email;

  private int phoneNumber;

  private int numBooks;

  private double debt;

  public Person(String name, String lastname, String address, String email, int phoneNumber) {
    this.uuid = UUID.randomUUID();
    this.name = name;
    this.lastname = lastname;
    this.address = address;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.numBooks = DEFAULT_NUM_BOOKS;
    this.debt = DEFAULT_DEBT;
  }

  public Person(UUID id, String name, String lastname, String address, String email, int phoneNumber) {
    this.uuid = id;
    this.name = name;
    this.lastname = lastname;
    this.address = address;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.numBooks = DEFAULT_NUM_BOOKS;
    this.debt = DEFAULT_DEBT;
  }

  public Person(UUID id, String name, String lastname, String address, String email, int phoneNumber, double debt) {
    this.uuid = id;
    this.name = name;
    this.lastname = lastname;
    this.address = address;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.numBooks = DEFAULT_NUM_BOOKS;
    this.debt = debt;
  }

  public Person(String id, String name, String lastname, String address, String email, int phoneNumber) {
    if (invalidId(id)) {
      throw new NotUniqueIdException();
    }
    this.uuid = UUID.fromString(id);
    this.name = name;
    this.lastname = lastname;
    this.address = address;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.numBooks = DEFAULT_NUM_BOOKS;
    this.debt = DEFAULT_DEBT;
  }

  @Override
  public UUID getId() {
    return uuid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(int phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public int getNumBooks() {
    return numBooks;
  }

  public void setNumBooks(int numBooks) {
    this.numBooks = numBooks;
  }

  public double getDebt() {
    return debt;
  }

  public void setDebt(double debt) {
    this.debt = debt;
  }

  @Override
  public void takeMaterial() {
    numBooks++;
  }

  @Override
  public void returnMaterial() {
    if (numBooks == 0) {
      throw new NotMoreMaterialsToReturnException();
    } else {
      numBooks--;
    }
  }

  @Override
  public int compareTo(Person o) {
    return this.getId().toString().compareTo(o.getId().toString());
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof Person person)) {
      return false;
    }
    return this.getId().toString().equals(person.getId().toString());
  }

  @Override
  public String toString() {
    return """
            ID:               %s
            Name:             %s
            Lastname:         %s
            Address:          %s
            Email:            %s
            Phone Number:     %s
            Number of books:  %d
            Debt:             %f"""
            .formatted(uuid.toString(), name, lastname, address, email, phoneNumber, numBooks, debt);
  }
}

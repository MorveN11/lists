package programming4.library.classes.persons;

import java.util.UUID;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.classes.persons.interfaces.IStudent;
import programming4.library.utils.Ui;

public class Student extends Person implements IStudent {

  public Student(String name, String lastname, String address, String email, int phoneNumber) {
    super(name, lastname, address, email, phoneNumber);
  }

  public Student(UUID id, String name, String lastname, String address, String email, int phoneNumber) {
    super(id, name, lastname, address, email, phoneNumber);
  }

  public Student(String id, String name, String lastname, String address, String email, int phoneNumber) {
    super(id, name, lastname, address, email, phoneNumber);
  }

  @Override
  public String toString() {
    return """
            %s
            %s
            %s
            """.formatted(Ui.printTitle("Student"), super.toString(), Ui.printLine());
  }
}

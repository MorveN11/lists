package programming4.library.classes.loans;

import static programming4.library.utils.Constants.DEFAULT_NUM_LOAN_MONTHS;

import java.time.LocalDate;
import java.util.UUID;
import programming4.library.classes.interfaces.Record;
import programming4.library.classes.loans.interfaces.ILoan;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.errors.NotValidIdException;
import programming4.library.utils.Ui;

public class Loan implements Record, ILoan, Comparable<Loan> {

  private final UUID loanID;

  private Material material;

  private Person person;

  private LocalDate loanDate;

  private LocalDate returnDate;

  public Loan(Material material, Person person, LocalDate loanDate, LocalDate returnDate) {
    this.loanID = UUID.randomUUID();
    this.material = material;
    this.person = person;
    this.loanDate = loanDate;
    this.returnDate = returnDate;
  }

  public Loan(Material material, Person person) {
    this.loanID = UUID.randomUUID();
    this.material = material;
    this.person = person;
    this.loanDate = LocalDate.now();
    this.returnDate = loanDate.plusMonths(DEFAULT_NUM_LOAN_MONTHS);
  }

  public Loan(String id, Material material, Person person) {
    if (invalidId(id)) {
      throw new NotValidIdException();
    }
    this.loanID = UUID.fromString(id);
    this.material = material;
    this.person = person;
    this.loanDate = LocalDate.now();
    this.returnDate = loanDate.plusMonths(DEFAULT_NUM_LOAN_MONTHS);
  }

  @Override
  public UUID getId() {
    return loanID;
  }

  public Material getMaterial() {
    return material;
  }

  public void setMaterial(Material material) {
    this.material = material;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public LocalDate getLoanDate() {
    return loanDate;
  }

  public void setLoanDate(LocalDate loanDate) {
    this.loanDate = loanDate;
  }

  public LocalDate getReturnDate() {
    return returnDate;
  }

  public void setReturnDate(LocalDate returnDate) {
    this.returnDate = returnDate;
  }

  @Override
  public int compareTo(Loan o) {
    return this.loanID.toString().compareTo(o.loanID.toString());
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof Loan loan)) {
      return false;
    }
    return this.getId().toString().equals(loan.getId().toString());
  }

  @Override
  public String toString() {
    return String.format("""
                    %s
                    Loan ID:       %s
                    Material:      %s - %s
                    Person:        %s %s
                    Loan Date :    %s
                    Return Date:   %s
                    %s
                    """, Ui.printTitle("Loan"), loanID, material.getTitle(), material.getAuthor(), person.getName(),
            person.getLastname(), loanDate, returnDate, Ui.printLine());
  }
}

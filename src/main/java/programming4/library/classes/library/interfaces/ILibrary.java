package programming4.library.classes.library.interfaces;

import programming4.library.treeList.loans.LoansTreeList;
import programming4.library.treeList.materials.MaterialsTreeList;
import programming4.library.treeList.persons.PersonsTreeList;

public interface ILibrary {

  void loadPersons(PersonsTreeList persons);

  void loadMaterials(MaterialsTreeList materials);

  void loadLoans(LoansTreeList loans);
}

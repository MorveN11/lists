package programming4.library.classes.library;

import programming4.library.classes.library.interfaces.ILibrary;
import programming4.library.treeList.loans.LoansTreeList;
import programming4.library.treeList.materials.MaterialsTreeList;
import programming4.library.treeList.persons.PersonsTreeList;
import programming4.library.utils.Ui;

public class Library implements ILibrary {

  private PersonsTreeList persons;

  private MaterialsTreeList materials;

  private LoansTreeList loans;

  public Library() {
    this.persons = new PersonsTreeList();
    this.materials = new MaterialsTreeList();
    this.loans = new LoansTreeList();
  }

  public PersonsTreeList getPersons() {
    return this.persons;
  }

  public MaterialsTreeList getMaterials() {
    return this.materials;
  }

  public LoansTreeList getLoans() {
    return this.loans;
  }

  @Override
  public void loadPersons(PersonsTreeList persons) {
    this.persons = persons;
  }

  @Override
  public void loadMaterials(MaterialsTreeList materials) {
    this.materials = materials;
  }

  @Override
  public void loadLoans(LoansTreeList loans) {
    this.loans = loans;
  }

  @Override
  public String toString() {
    return """
            %s
            Persons: %s
            Materials: %s
            Loans: %s
            %s
            """.formatted(Ui.printTitle("Library"), persons, materials, loans, Ui.printLine());
  }
}

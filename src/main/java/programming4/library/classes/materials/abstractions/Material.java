package programming4.library.classes.materials.abstractions;

import java.util.UUID;
import programming4.library.classes.interfaces.Record;
import programming4.library.classes.materials.abstractions.interfaces.IMaterial;
import programming4.library.errors.NotImplementedFunctionException;
import programming4.library.errors.NotUniqueIdException;

public class Material implements Record, IMaterial, Comparable<Material> {

  private final UUID id;

  private String title;

  private String author;

  private String category;

  private String isbn;

  private int publicationYear;

  private int pageCount;

  private boolean available;

  private int quantity;

  private String typeMaterial;

  public Material(String title, String author, String category, String isbn, int publicationYear,
                  int pageCount, boolean available, int quantity, String typeMaterial) {
    this.id = UUID.randomUUID();
    this.title = title;
    this.author = author;
    this.category = category;
    this.isbn = isbn;
    this.publicationYear = publicationYear;
    this.pageCount = pageCount;
    this.available = available;
    this.quantity = quantity;
    this.typeMaterial = typeMaterial;
  }

  public Material(UUID id, String title, String author, String category, String isbn, int publicationYear,
                  int pageCount, boolean available, int quantity, String typeMaterial) {
    this.id = id;
    this.title = title;
    this.author = author;
    this.category = category;
    this.isbn = isbn;
    this.publicationYear = publicationYear;
    this.pageCount = pageCount;
    this.available = available;
    this.quantity = quantity;
    this.typeMaterial = typeMaterial;
  }

  public Material(String id, String title, String author, String category, String isbn, int publicationYear,
                  int pageCount, boolean available, int quantity, String typeMaterial) {
    if (invalidId(id)) {
      throw new NotUniqueIdException();
    }
    this.id = UUID.fromString(id);
    this.title = title;
    this.author = author;
    this.category = category;
    this.isbn = isbn;
    this.publicationYear = publicationYear;
    this.pageCount = pageCount;
    this.available = available;
    this.quantity = quantity;
    this.typeMaterial = typeMaterial;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public int getPublicationYear() {
    return publicationYear;
  }

  public void setPublicationYear(int publicationYear) {
    this.publicationYear = publicationYear;
  }

  public int getPageCount() {
    return pageCount;
  }

  public void setPageCount(int pageCount) {
    this.pageCount = pageCount;
  }

  public boolean isAvailable() {
    return available;
  }

  public void setAvailable(boolean available) {
    this.available = available;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public String getTypeMaterial() {
    return typeMaterial;
  }

  public void setTypeMaterial(String typeMaterial) {
    this.typeMaterial = typeMaterial;
  }

  @Override
  public void highMaterial() {
    throw new NotImplementedFunctionException();
  }

  @Override
  public void lowMaterial() {
    throw new NotImplementedFunctionException();
  }

  @Override
  public void changeMaterial() {
    throw new NotImplementedFunctionException();
  }

  @Override
  public int compareTo(Material o) {
    return id.toString().compareTo(o.getId().toString());
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof Material material)) {
      return false;
    }
    return this.getId().toString().equals(material.getId().toString());
  }

  @Override
  public String toString() {
    return """
            ID:               %s
            Title:            %s
            Author:           %s
            Category:         %s
            ISBN:             %s
            Publication Year: %d
            Page Count:       %d
            Available:        %b
            Quantity:         %d
            Type Material:    %s"""
            .formatted(id, title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
  }
}

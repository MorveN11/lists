package programming4.library.classes.materials.abstractions.interfaces;

public interface IMaterial {

  void highMaterial();

  void lowMaterial();

  void changeMaterial();
}

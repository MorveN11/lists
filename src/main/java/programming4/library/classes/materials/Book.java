package programming4.library.classes.materials;

import java.util.UUID;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.classes.materials.interfaces.IBook;
import programming4.library.utils.Ui;

public class Book extends Material implements IBook {

  private String editorial;

  public Book(String title, String author, String category, String isbn, int publicationYear, int pageCount,
              boolean available, int quantity, String typeMaterial, String editorial) {
    super(title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
    this.editorial = editorial;
  }

  public Book(UUID id, String title, String author, String category, String isbn, int publicationYear, int pageCount,
              boolean available, int quantity, String typeMaterial, String editorial) {
    super(id, title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
    this.editorial = editorial;
  }

  public Book(String id, String title, String author, String category, String isbn, int publicationYear, int pageCount,
              boolean available, int quantity, String typeMaterial, String editorial) {
    super(id, title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
    this.editorial = editorial;
  }

  public String getEditorial() {
    return editorial;
  }

  public void setEditorial(String editorial) {
    this.editorial = editorial;
  }

  @Override
  public String toString() {
    return """
            %s
            %s
            Editorial:        %s
            %s
            """.formatted(Ui.printTitle("Book"), super.toString(), this.editorial, Ui.printLine());
  }
}

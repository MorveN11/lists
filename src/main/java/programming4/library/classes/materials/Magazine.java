package programming4.library.classes.materials;

import java.util.UUID;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.classes.materials.interfaces.IMagazine;
import programming4.library.utils.Ui;

public class Magazine extends Material implements IMagazine {

  public Magazine(String title, String author, String category, String isbn,
                  int publicationYear, int pageCount, boolean available, int quantity, String typeMaterial) {
    super(title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
  }

  public Magazine(UUID id, String title, String author, String category, String isbn,
                  int publicationYear, int pageCount, boolean available, int quantity, String typeMaterial) {
    super(id, title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
  }

  public Magazine(String id, String title, String author, String category, String isbn,
                  int publicationYear, int pageCount, boolean available, int quantity, String typeMaterial) {
    super(id, title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
  }

  @Override
  public String toString() {
    return """
            %s
            %s
            %s
            """.formatted(Ui.printTitle("Magazine"), super.toString(), Ui.printLine());
  }
}

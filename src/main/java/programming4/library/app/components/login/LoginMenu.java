package programming4.library.app.components.login;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.components.admin.AdminMenu;
import programming4.library.app.components.user.UserMenu;
import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.library.Library;
import programming4.library.utils.Ui;

public class LoginMenu implements OptionalComponent {

  private final AdminMenu adminMenu;

  private final UserMenu userMenu;

  private final BufferedReader br;

  private final BufferedWriter bw;

  public LoginMenu(Library library, BufferedReader br, BufferedWriter bw) {
    this.adminMenu = new AdminMenu(library, br, bw);
    this.userMenu = new UserMenu(library, br, bw);
    this.br = br;
    this.bw = bw;
  }

  @Override
  public void initComponent() throws IOException {
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = validateMenuOption();
      switch (option) {
        case 1 -> adminMenu.initComponent();
        case 2 -> userMenu.initComponent();
        case 3 -> flag = true;
        case 4 -> {
          return;
        }
        case 5 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
  }

  @Override
  public BufferedReader getReader() {
    return this.br;
  }

  @Override
  public BufferedWriter getWriter() {
    return this.bw;
  }

  @Override
  public String toString() {
    return """
            %s
            1. Login As Administrator
            2. Login As User
            3. Exit Application
            %s
            """.formatted(Ui.printTitle("Login Menu"), Ui.printLine());
  }
}

package programming4.library.app.components.common.components.person;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.components.admin.components.abstraction.CrudMenu;
import programming4.library.app.components.admin.components.abstraction.RecordOptionMenu;
import programming4.library.app.components.admin.components.abstraction.enums.ChildRecordEnum;
import programming4.library.app.components.admin.components.abstraction.enums.RecordEnum;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.errors.NotFoundException;
import programming4.library.treeList.persons.PersonsTreeList;

public class PersonCrud extends CrudMenu<Person> {

  private final PersonsTreeList persons;

  private final RecordOptionMenu recordOptionMenu;

  public PersonCrud(PersonsTreeList recordsList, BufferedReader br, BufferedWriter bw) {
    super(recordsList, RecordEnum.Person, br, bw);
    this.persons = recordsList;
    this.recordOptionMenu = new RecordOptionMenu(br, bw, RecordEnum.Person);
  }

  private Person verifyExistsPerson(String id, ChildRecordEnum option) throws IOException {
    Person person;
    try {
      person = persons.getByIdByTree(id);
    } catch (NotFoundException e) {
      write(e.getMessage());
      return null;
    }
    if (option == ChildRecordEnum.Student && !(person instanceof Student)) {
      write("Any Student was found with the id: " + id);
      return null;
    } else if (option == ChildRecordEnum.Teacher && !(person instanceof Teacher)) {
      write("Any Teacher was found with the id: " + id);
      return null;
    }
    return person;
  }

  @Override
  protected void createRecord() throws IOException {
    ChildRecordEnum option = recordOptionMenu.initReturnComponent();
    if (option == null) return;
    String name = readString("Enter the Name: ");
    if (name == null) return;
    String lastname = readString("Enter the Lastname: ");
    if (lastname == null) return;
    String address = readString("Enter the Address: ");
    if (address == null) return;
    String email = readEmail("Enter the Email: ");
    if (email == null) return;
    Integer phoneNumber = readPhoneNumber("Enter the Phone Number: ");
    if (phoneNumber == null) return;
    Person person = option == ChildRecordEnum.Student ?
            new Student(name, lastname, address, email, phoneNumber)
            : new Teacher(name, lastname, address, email, phoneNumber);
    persons.add(person);
    getWriter().write("The " + option + " was created successfully\n");
    getWriter().flush();
  }

  @Override
  protected void readRecord() throws IOException {
    ChildRecordEnum option = recordOptionMenu.initReturnComponent();
    if (option == null) return;
    String id = readUuid("Enter the ID: ");
    if (id == null) return;
    Person person = verifyExistsPerson(id, option);
    if (person == null) {
      write(new NotFoundException().getMessage());
      return;
    }
    getWriter().write(person.toString());
    getWriter().write("The " + option + " was read successfully\n");
    getWriter().flush();
  }

  @Override
  protected void updateRecord() throws IOException {
    ChildRecordEnum option = recordOptionMenu.initReturnComponent();
    if (option == null) return;
    String id = readUuid("Enter the ID: ");
    if (id == null) return;
    String name = readString("Enter the New Name: ");
    if (name == null) return;
    String lastname = readString("Enter the New Lastname: ");
    if (lastname == null) return;
    String address = readString("Enter the New Address: ");
    if (address == null) return;
    String email = readEmail("Enter the New Email: ");
    if (email == null) return;
    Integer phoneNumber = readPhoneNumber("Enter the New Phone Number: ");
    if (phoneNumber == null) return;
    Person person = verifyExistsPerson(id, option);
    if (person == null) {
      write(new NotFoundException().getMessage());
      return;
    }
    Person updatePerson = option == ChildRecordEnum.Student ?
            new Student(name, lastname, address, email, phoneNumber)
            : new Teacher(name, lastname, address, email, phoneNumber);
    persons.updateById(person.getId(), updatePerson);
    getWriter().write("The " + option + " was updated successfully\n");
    getWriter().flush();
  }
}

package programming4.library.app.components.user.components.loan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.library.Library;
import programming4.library.classes.loans.Loan;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.errors.NotFoundException;
import programming4.library.list.loans.LoansList;
import programming4.library.treeList.materials.MaterialsTreeList;
import programming4.library.utils.Ui;

public class LoanMenu implements OptionalComponent {

  private final FilterMenu filterMenu;

  private final Library library;

  private final MaterialsTreeList materialsList;

  private final BufferedReader br;

  private final BufferedWriter bw;

  public LoanMenu(MaterialsTreeList materialsList, Library library, BufferedReader br, BufferedWriter bw) {
    this.filterMenu = new FilterMenu(library.getMaterials(), br, bw);
    this.library = library;
    this.materialsList = materialsList;
    this.br = br;
    this.bw = bw;
  }

  public void borrowMaterial() throws IOException {
    Person person;
    Material material;
    String personId = readUuid("Enter the person ID: ");
    if (personId == null) {
      return;
    }
    String materialId = readUuid("Enter the %s ID: ".formatted("Material"));
    if (materialId == null) {
      return;
    }
    try {
      person = library.getPersons().getByIdByTree(personId);
      material = library.getMaterials().getByIdByTree(materialId);
    } catch (NotFoundException e) {
      write(e.getMessage());
      return;
    }
    Loan loan = new Loan(material, person);
    Loan loanAdded = library.getLoans().add(loan);
    write("The material was borrowed successfully");
    write(loanAdded.toString());
  }

  public void loanHistory() throws IOException {
    String personId = readUuid("Enter the person ID: ");
    if (personId == null) {
      return;
    }
    LoansList loans = library.getLoans().filterByPersonId(personId);
    printRecordList(loans);
  }

  @Override
  public void initComponent() throws IOException {
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = validateMenuOption();
      switch (option) {
        case 1 -> printRecordList(materialsList);
        case 2 -> this.filterMenu.initComponent();
        case 3 -> borrowMaterial();
        case 4 -> loanHistory();
        case 5 -> flag = true;
        case 6 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
  }

  @Override
  public BufferedReader getReader() {
    return this.br;
  }

  @Override
  public BufferedWriter getWriter() {
    return this.bw;
  }

  @Override
  public String toString() {
    return """          
            %s
            1. Show Materials
            2. Filter Materials
            3. Select A Material To Borrow
            4. Show My Loans
            5. Back
            6. Exit Application
            %s
            """.formatted(Ui.printTitle("Loan Menu"), Ui.printLine());
  }
}

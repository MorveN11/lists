package programming4.library.app.components.user.components.loan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.list.materials.MaterialsList;
import programming4.library.treeList.materials.MaterialsTreeList;
import programming4.library.utils.Ui;

public class FilterMenu implements OptionalComponent {

  private final MaterialsTreeList materialsList;

  private final BufferedReader br;

  private final BufferedWriter bw;

  public FilterMenu(MaterialsTreeList materialsList, BufferedReader br, BufferedWriter bw) {
    this.materialsList = materialsList;
    this.br = br;
    this.bw = bw;
  }

  public String filteringDataString(String data) throws IOException {
    return readString("Enter the " + data + " of the material: ");
  }

  public String filteringDataISBN(String data) throws IOException {
    return readIsbn("Enter the " + data + " of the material: ");
  }

  public Integer filteringDataYear(String data) throws IOException {
    return readYear("Enter the " + data + " of the material: ");
  }

  public void checkElementExist(MaterialsList<Material> data) throws IOException {
    if (data.isEmpty()) {
      write("Not material founded");
    }
    printRecordList(data);
  }

  @Override
  public void initComponent() throws IOException {
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = validateMenuOption();
      String filteredData;
      switch (option) {
        case 1 -> {
          filteredData = filteringDataString("title");
          if (filteredData == null) continue;
          checkElementExist(materialsList.filterByTitle(filteredData));
        }
        case 2 -> {
          filteredData = filteringDataString("author");
          if (filteredData == null) continue;
          checkElementExist(materialsList.filterByAuthor(filteredData));
        }
        case 3 -> {
          filteredData = filteringDataString("category");
          if (filteredData == null) continue;
          checkElementExist(materialsList.filterByCategory(filteredData));
        }
        case 4 -> {
          filteredData = filteringDataISBN("ISBN");
          if (filteredData == null) continue;
          checkElementExist(materialsList.filterByIsbn(filteredData));
        }
        case 5 -> {
          Integer data = filteringDataYear("year");
          if (data == null) continue;
          checkElementExist(materialsList.filterByPublicationYear(data));
        }
        case 6 -> {
          filteredData = filteringDataString("type material");
          if (filteredData == null) continue;
          checkElementExist(materialsList.filterByTypeMaterial(filteredData));
        }
        case 7 -> flag = true;
        case 8 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
  }

  @Override
  public BufferedReader getReader() {
    return this.br;
  }

  @Override
  public BufferedWriter getWriter() {
    return this.bw;
  }

  @Override
  public String toString() {
    return """          
            %s
            1. Filter by Title
            2. Filter by Author
            3. Filter by Category
            4. Filter by ISBN
            5. Filter by Publication Year
            6. Filter by Type Material
            7. Back
            8. Exit Application
            %s
            """.formatted(Ui.printTitle("Loan Menu"), Ui.printLine());
  }
}

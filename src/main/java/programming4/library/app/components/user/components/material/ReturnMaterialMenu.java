package programming4.library.app.components.user.components.material;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.library.Library;
import programming4.library.list.loans.LoansList;
import programming4.library.utils.Ui;

public class ReturnMaterialMenu implements OptionalComponent {

  private final Library library;

  private final BufferedReader br;

  private final BufferedWriter bw;

  public ReturnMaterialMenu(Library library, BufferedReader br, BufferedWriter bw) {
    this.library = library;
    this.br = br;
    this.bw = bw;
  }

  public void myLoans() throws IOException {
    String personId = readUuid("Enter the loan ID: ");
    if (personId == null) {
      return;
    }
    LoansList loans = library.getLoans().filterByPersonId(personId);
    printRecordList(loans);
  }

  @Override
  public BufferedReader getReader() {
    return this.br;
  }

  @Override
  public BufferedWriter getWriter() {
    return this.bw;
  }

  @Override
  public void initComponent() throws IOException {
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = validateMenuOption();
      switch (option) {
        case 1 -> myLoans();
        case 2 -> {
          String personId = readUuid("Enter the Loan ID: ");
          if (personId == null) {
            return;
          }
          library.getLoans().removeById(personId);
        }
        case 3 -> flag = true;
        case 4 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
  }

  @Override
  public String toString() {
    return """
            %s
            1. Show My loans
            2. Return Material
            3. Back
            4. Exit Application
            %s
            """.formatted(Ui.printTitle("Menu Admin"), Ui.printLine());
  }
}

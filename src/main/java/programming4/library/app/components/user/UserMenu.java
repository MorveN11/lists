package programming4.library.app.components.user;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import programming4.library.app.components.user.components.fines.FinesMenu;
import programming4.library.app.components.user.components.loan.LoanMenu;
import programming4.library.app.components.user.components.material.ReturnMaterialMenu;
import programming4.library.app.components.user.components.person.UpdatePerson;
import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.library.Library;
import programming4.library.utils.Ui;

public class UserMenu implements OptionalComponent {

  private final UpdatePerson updatePerson;

  private final LoanMenu loanMenu;

  private final ReturnMaterialMenu returnMaterialMenu;

  private final FinesMenu finesMenu;

  private final BufferedReader br;

  private final BufferedWriter bw;

  public UserMenu(Library library, BufferedReader br, BufferedWriter bw) {
    this.updatePerson = new UpdatePerson(library.getPersons(),br,bw);
    this.loanMenu = new LoanMenu(library.getMaterials(), library, br, bw);
    this.returnMaterialMenu = new ReturnMaterialMenu(library, br, bw);
    this.finesMenu = new FinesMenu(library, br,bw);
    this.br = br;
    this.bw = bw;
  }

  @Override
  public void initComponent() throws IOException {
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = validateMenuOption();
      switch (option) {
        case 1 -> this.loanMenu.initComponent();
        case 2 -> this.returnMaterialMenu.initComponent();
        case 3 -> this.updatePerson.initComponent();
        case 4 -> this.finesMenu.initComponent();
        case 5 -> flag = true;
        case 6 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
  }

  @Override
  public BufferedReader getReader() {
    return this.br;
  }

  @Override
  public BufferedWriter getWriter() {
    return this.bw;
  }

  @Override
  public String toString() {
    return """
            %s
            1. Loans
            2. Return Material
            3. Update My Information
            4. Fines
            5. Back
            6. Exit Application
            %s
            """.formatted(Ui.printTitle("Menu User"), Ui.printLine());
  }
}

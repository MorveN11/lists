package programming4.library.app.components.user.components.fines;

import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.library.Library;
import programming4.library.list.loans.LoansList;
import programming4.library.utils.Ui;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class FinesMenu implements OptionalComponent {
    private final Library library;

    private final BufferedReader br;

    private final BufferedWriter bw;

    public FinesMenu(Library library, BufferedReader br, BufferedWriter bw) {
        this.library = library;
        this.br = br;
        this.bw = bw;
    }

    public void showFines() throws IOException {
        String personID = readUuid("Enter the person ID: ");
        LoansList loansSize = library.getLoans().filterByPersonId(personID);
        for (int i = 0; i < loansSize.size(); i++) {
                LocalDate currentDate = LocalDate.now();
                LocalDate returnDate = loansSize.getByIndex(i).getReturnDate();
                if (currentDate.isAfter(returnDate)) {
                    long daysLate = ChronoUnit.DAYS.between(returnDate, currentDate);
                    int fineAmountPerDay = 5;
                    double totalFine = fineAmountPerDay * (int) daysLate;
                    double actualFine = library.getPersons().getByIdByTree(personID).getDebt();
                    library.getPersons().getByIdByTree(personID).setDebt(totalFine + actualFine);
                    write("You have a fine of " + totalFine + " units of currency for being " + daysLate + " days late.");
                } else {
                    write("You don't have any debt. Enjoy your reading!");
                }
        }
    }

    @Override
    public void initComponent() throws IOException {
        boolean flag = false;
        while (!flag) {
            this.printComponent();
            int option = validateMenuOption();
            switch (option) {
                case 1 -> showFines();
                case 2 -> flag = true;
                case 3 -> exitApplication();
                default -> bw.write(Ui.invalidInput());
            }
        }
    }


    @Override
    public BufferedReader getReader() {
        return this.br;
    }

    @Override
    public BufferedWriter getWriter() {
        return this.bw;
    }

    @Override
    public String toString() {
        return """          
            %s
            1. Show Your Fines
            2. Back
            3. Exit Application
            %s
            """.formatted(Ui.printTitle("Fines Menu"), Ui.printLine());
    }
}

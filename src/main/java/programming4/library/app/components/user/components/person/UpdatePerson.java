package programming4.library.app.components.user.components.person;

import programming4.library.app.components.common.components.person.PersonCrud;
import programming4.library.treeList.persons.PersonsTreeList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class UpdatePerson extends PersonCrud {
    public UpdatePerson(PersonsTreeList recordsList, BufferedReader br, BufferedWriter bw) {
        super(recordsList, br, bw);
    }

    @Override
    public void initComponent() throws IOException {
        updateRecord();
    }
}

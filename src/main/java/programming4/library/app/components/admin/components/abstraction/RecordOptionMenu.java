package programming4.library.app.components.admin.components.abstraction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import programming4.library.app.components.admin.components.abstraction.enums.ChildRecordEnum;
import programming4.library.app.components.admin.components.abstraction.enums.RecordEnum;
import programming4.library.app.interfaces.ReturnComponent;
import programming4.library.utils.Ui;

public class RecordOptionMenu implements ReturnComponent<ChildRecordEnum> {

  private final BufferedReader br;

  private final BufferedWriter bw;

  private final RecordEnum record;

  private final Map<RecordEnum, ChildRecordEnum[]> map = new HashMap<>() {
    {
      put(RecordEnum.Person, new ChildRecordEnum[]{ChildRecordEnum.Student, ChildRecordEnum.Teacher});
      put(RecordEnum.Material, new ChildRecordEnum[]{ChildRecordEnum.Book, ChildRecordEnum.Magazine});
    }
  };

  public RecordOptionMenu(BufferedReader br, BufferedWriter bw, RecordEnum record) {
    this.br = br;
    this.bw = bw;
    this.record = record;
  }

  @Override
  public ChildRecordEnum initReturnComponent() throws IOException {
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = this.validateMenuOption();
      switch (option) {
        case 1 -> {
          return map.get(record)[0];
        }
        case 2 -> {
          return map.get(record)[1];
        }
        case 3 -> flag = true;
        case 4 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
    return null;
  }

  @Override
  public BufferedReader getReader() {
    return this.br;
  }

  @Override
  public BufferedWriter getWriter() {
    return this.bw;
  }

  @Override
  public String toString() {
    return """
            %s
            1. %s
            2. %s
            3. Back
            4. Exit Application
            %s
            """.formatted(Ui.printTitle("Select the type of " + record),
            map.get(record)[0], map.get(record)[1], Ui.printLine());
  }
}

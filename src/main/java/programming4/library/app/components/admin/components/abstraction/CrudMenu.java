package programming4.library.app.components.admin.components.abstraction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.components.admin.components.abstraction.enums.ChildRecordEnum;
import programming4.library.app.components.admin.components.abstraction.enums.RecordEnum;
import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.interfaces.Record;
import programming4.library.errors.NotFoundException;
import programming4.library.treeList.records.RecordsTreeList;
import programming4.library.utils.Ui;

public abstract class CrudMenu<T extends Record & Comparable<T>> implements OptionalComponent {

  private final RecordEnum record;

  private final BufferedReader br;

  private final BufferedWriter bw;

  private final RecordsTreeList<T> recordsList;

  private final RecordOptionMenu recordOptionMenu;

  public CrudMenu(RecordsTreeList<T> recordsList, RecordEnum record, BufferedReader br, BufferedWriter bw) {
    this.record = record;
    this.br = br;
    this.bw = bw;
    this.recordsList = recordsList;
    this.recordOptionMenu = new RecordOptionMenu(br, bw, record);
  }

  protected abstract void createRecord() throws IOException;

  protected abstract void readRecord() throws IOException;

  protected abstract void updateRecord() throws IOException;

  protected void removeRecord() throws IOException {
    ChildRecordEnum option = recordOptionMenu.initReturnComponent();
    if (option == null) return;
    String id = readUuid("Enter the ID: ");
    if (id == null) {
      return;
    }
    try {
      recordsList.removeById(id);
    } catch (NotFoundException e) {
      write(e.getMessage());
    }
    getWriter().write("The " + option + " was removed successfully\n");
    getWriter().flush();
  }

  @Override
  public BufferedReader getReader() {
    return br;
  }

  @Override
  public BufferedWriter getWriter() {
    return bw;
  }

  @Override
  public void initComponent() throws IOException {
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = this.validateMenuOption();
      switch (option) {
        case 1 -> createRecord();
        case 2 -> readRecord();
        case 3 -> updateRecord();
        case 4 -> removeRecord();
        case 5 -> flag = true;
        case 6 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
  }

  @Override
  public String toString() {
    return """
            %s
            1. Add %s
            2. Show %s
            3. Update %s
            4. Remove %s
            5. Back
            6. Exit Application
            %s
            """.formatted(Ui.printTitle(record.toString()), record, record, record, record, Ui.printLine());
  }
}

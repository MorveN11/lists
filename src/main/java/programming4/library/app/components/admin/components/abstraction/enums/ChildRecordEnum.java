package programming4.library.app.components.admin.components.abstraction.enums;

public enum ChildRecordEnum {
  Student,
  Teacher,
  Book,
  Magazine
}

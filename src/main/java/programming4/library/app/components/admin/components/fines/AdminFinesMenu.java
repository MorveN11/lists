package programming4.library.app.components.admin.components.fines;

import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.library.Library;
import programming4.library.utils.Ui;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class AdminFinesMenu implements OptionalComponent {
    private final Library library;

    private final BufferedReader br;

    private final BufferedWriter bw;

    public AdminFinesMenu(Library library, BufferedReader br, BufferedWriter bw) {
        this.library = library;
        this.br = br;
        this.bw = bw;
    }

    @Override
    public void initComponent() throws IOException {
        boolean flag = false;
        while (!flag) {
            this.printComponent();
            int option = validateMenuOption();
            switch (option) {
                case 1 -> printRecordList(this.library.getPersons());
                case 2 -> flag = true;
                case 3 -> exitApplication();
                default -> bw.write(Ui.invalidInput());
            }
        }
    }

    @Override
    public BufferedReader getReader() {
        return this.br;
    }

    @Override
    public BufferedWriter getWriter() {
        return this.bw;
    }

    @Override
    public String toString() {
        return """          
            %s
            1. Global Fines
            2. Back
            3. Exit Application
            %s
            """.formatted(Ui.printTitle("Fines Menu"), Ui.printLine());
    }
}

package programming4.library.app.components.admin.components.material;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.components.admin.components.abstraction.CrudMenu;
import programming4.library.app.components.admin.components.abstraction.RecordOptionMenu;
import programming4.library.app.components.admin.components.abstraction.enums.ChildRecordEnum;
import programming4.library.app.components.admin.components.abstraction.enums.RecordEnum;
import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.errors.NotFoundException;
import programming4.library.treeList.materials.MaterialsTreeList;

public class MaterialCrudMenu extends CrudMenu<Material> {

  private final MaterialsTreeList materials;

  private final RecordOptionMenu recordOptionMenu;

  public MaterialCrudMenu(MaterialsTreeList recordsList, BufferedReader br, BufferedWriter bw) {
    super(recordsList, RecordEnum.Material, br, bw);
    this.materials = recordsList;
    this.recordOptionMenu = new RecordOptionMenu(br, bw, RecordEnum.Material);
  }

  private Material verifyExistsMaterial(String id, ChildRecordEnum option) throws IOException {
    Material material;
    try {
      material = materials.getByIdByTree(id);
    } catch (NotFoundException e) {
      write(e.getMessage());
      return null;
    }
    if (option == ChildRecordEnum.Book && !(material instanceof Book)) {
      write("Any Book was found with the id: " + id);
      return null;
    } else if (option == ChildRecordEnum.Magazine && !(material instanceof Magazine)) {
      write("Any Magazine was found with the id: " + id);
      return null;
    }
    return material;
  }

  @Override
  protected void createRecord() throws IOException {
    ChildRecordEnum option = recordOptionMenu.initReturnComponent();
    if (option == null) return;
    String title = readString("Enter the Title: ");
    if (title == null) return;
    String author = readString("Enter the Author: ");
    if (author == null) return;
    String category = readString("Enter the Category: ");
    if (category == null) return;
    String isbn = readIsbn("Enter the ISBN: ");
    if (isbn == null) return;
    Integer publicationYear = readYear("Enter the Publication Year: ");
    if (publicationYear == null) return;
    Integer pageCount = readInt("Enter the Page Count: ");
    if (pageCount == null) return;
    Boolean available = readBoolean("Enter the Available: ");
    if (available == null) return;
    Integer quantity = readInt("Enter the Quantity: ");
    if (quantity == null) return;
    String typeMaterial = readString("Enter the Type Material: ");
    if (typeMaterial == null) return;
    String editorial = "";
    if (option == ChildRecordEnum.Book) {
      editorial = readString("Enter the Editorial: ");
      if (editorial == null) return;
    }
    Material material = option == ChildRecordEnum.Book
            ? new Book(title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial, editorial)
            : new Magazine(title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
    materials.add(material);
    getWriter().write("The " + option + " was created successfully\n");
    getWriter().flush();
  }

  @Override
  protected void readRecord() throws IOException {
    ChildRecordEnum option = recordOptionMenu.initReturnComponent();
    if (option == null) return;
    String id = readUuid("Enter the ID: ");
    if (id == null) return;
    Material material = verifyExistsMaterial(id, option);
    if (material == null) {
      write(new NotFoundException().getMessage());
      return;
    }
    getWriter().write(material.toString());
    getWriter().write("The " + option + " was read successfully\n");
    getWriter().flush();
  }

  @Override
  protected void updateRecord() throws IOException {
    ChildRecordEnum option = recordOptionMenu.initReturnComponent();
    if (option == null) return;
    String id = readUuid("Enter the ID: ");
    if (id == null) return;
    String title = readString("Enter the New Title: ");
    if (title == null) return;
    String author = readString("Enter the New Author: ");
    if (author == null) return;
    String category = readString("Enter the New Category: ");
    if (category == null) return;
    String isbn = readIsbn("Enter the New ISBN: ");
    if (isbn == null) return;
    Integer publicationYear = readYear("Enter the New Publication Year: ");
    if (publicationYear == null) return;
    Integer pageCount = readInt("Enter the New Page Count: ");
    if (pageCount == null) return;
    Boolean available = readBoolean("Enter the New Available: ");
    if (available == null) return;
    Integer quantity = readInt("Enter the New Quantity: ");
    if (quantity == null) return;
    String typeMaterial = readString("Enter the New Type Material: ");
    if (typeMaterial == null) return;
    String editorial = "";
    if (option == ChildRecordEnum.Book) {
      editorial = readString("Enter the New Editorial: ");
      if (editorial == null) return;
    }
    Material material = verifyExistsMaterial(id, option);
    if (material == null) {
      write(new NotFoundException().getMessage());
      return;
    }
    Material updateMaterial = option == ChildRecordEnum.Book
            ? new Book(title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial, editorial)
            : new Magazine(title, author, category, isbn, publicationYear, pageCount, available, quantity, typeMaterial);
    materials.updateById(material.getId(), updateMaterial);
    getWriter().write("The " + option + " was updated successfully\n");
    getWriter().flush();
  }
}

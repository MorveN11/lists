package programming4.library.app.components.admin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.components.admin.components.fines.AdminFinesMenu;
import programming4.library.app.components.admin.components.material.MaterialCrudMenu;
import programming4.library.app.components.admin.components.material.ReservedMaterialMenu;
import programming4.library.app.components.common.components.person.PersonCrud;
import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.classes.library.Library;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.list.materials.MaterialsList;
import programming4.library.utils.Ui;

public class AdminMenu implements OptionalComponent {

  private final Library library;

  private final AdminFinesMenu adminFinesMenu;

  private final MaterialCrudMenu materialCrud;

  private final PersonCrud personCrud;

  private final ReservedMaterialMenu reservedMaterialMenu;

  private final BufferedReader br;

  private final BufferedWriter bw;

  public AdminMenu(Library library, BufferedReader br, BufferedWriter bw) {
    this.library = library;
    this.materialCrud = new MaterialCrudMenu(library.getMaterials(), br, bw);
    this.personCrud = new PersonCrud(library.getPersons(), br, bw);
    this.adminFinesMenu = new AdminFinesMenu(library, br, bw);
    this.reservedMaterialMenu = new ReservedMaterialMenu(library.getMaterials(), br, bw);
    this.br = br;
    this.bw = bw;
  }

  @Override
  public BufferedReader getReader() {
    return this.br;
  }

  @Override
  public BufferedWriter getWriter() {
    return this.bw;
  }

  @Override
  public void initComponent() throws IOException {
    MaterialsList<Material> lowStack = library.getMaterials().filterByLowStock();
    if (!lowStack.isEmpty()) {
      write(Ui.printTitle("Low Stock"));
      printRecordList(lowStack);
      write(Ui.printLine());
    }
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = validateMenuOption();
      switch (option) {
        case 1 -> printRecordList(this.library.getPersons());
        case 2 -> printRecordList(this.library.getMaterials());
        case 3 -> printRecordList(this.library.getLoans());
        case 4 -> this.personCrud.initComponent();
        case 5 -> this.materialCrud.initComponent();
        case 6 -> this.reservedMaterialMenu.initComponent();
        case 7 -> this.adminFinesMenu.initComponent();
        case 8 -> flag = true;
        case 9 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
  }

  @Override
  public String toString() {
    return """
            %s
            1. Show Persons
            2. Show Materials
            3. Show Loans
            4. Manage Persons
            5. Manage Materials
            6. Manage Reserved materials
            7. Show User Fines
            8. Back
            9. Exit Application
            %s
            """.formatted(Ui.printTitle("Menu Admin"), Ui.printLine());
  }
}

package programming4.library.app.components.admin.components.material;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import programming4.library.app.interfaces.OptionalComponent;
import programming4.library.treeList.materials.MaterialsTreeList;
import programming4.library.utils.Ui;

public class ReservedMaterialMenu implements OptionalComponent {

  private final MaterialsTreeList materialsList;

  private final BufferedReader br;

  private final BufferedWriter bw;

  public ReservedMaterialMenu(MaterialsTreeList materialsList, BufferedReader br, BufferedWriter bw) {
    this.materialsList = materialsList;
    this.br = br;
    this.bw = bw;
  }

  @Override
  public void initComponent() throws IOException {
    boolean flag = false;
    while (!flag) {
      this.printComponent();
      int option = validateMenuOption();
      switch (option) {
        case 1 -> {
          MaterialsTreeList materialsTreeList = new MaterialsTreeList();
          for (int i = 0; i < materialsList.getList().size(); i++) {
            if (materialsList.getList().getByIndex(i).getQuantity() <= 0) {
              materialsTreeList.add(materialsList.getList().getByIndex(i));
            }
          }
          printRecordList(materialsTreeList);
        }
        case 2 -> flag = true;
        case 3 -> exitApplication();
        default -> write(Ui.invalidInput());
      }
    }
  }

  @Override
  public BufferedReader getReader() {
    return this.br;
  }

  @Override
  public BufferedWriter getWriter() {
    return this.bw;
  }

  @Override
  public String toString() {
    return """          
            %s
            1. Show reserved material
            2. Back
            3. Exit Application
            %s
            """.formatted(Ui.printTitle("Loan Menu"), Ui.printLine());
  }
}

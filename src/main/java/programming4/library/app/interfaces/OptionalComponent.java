package programming4.library.app.interfaces;

import java.io.IOException;
import programming4.library.classes.interfaces.Record;
import programming4.library.list.records.RecordsList;
import programming4.library.treeList.records.RecordsTreeList;
import programming4.library.utils.Ui;
import programming4.library.utils.ValidateInputs;

@SuppressWarnings("ALL")
public interface OptionalComponent extends Component {

  private String getInput() throws IOException {
    getWriter().write(Ui.cancelInput() + "\n");
    getWriter().flush();
    String stOption = getReader().readLine();
    stOption = stOption.trim();
    if (stOption.toLowerCase().equals("cancel")) {
      getWriter().write(Ui.canceledInput() + "\n");
      getWriter().flush();
      return null;
    }
    return stOption;
  }

  private void printComponentInvalidInput() throws IOException {
    getWriter().write(Ui.invalidInput() + "\n");
    this.printComponent();
  }

  private void printTitleInvalidInput(String title) throws IOException {
    getWriter().write(Ui.invalidInput() + "\n");
    getWriter().write(title);
    getWriter().flush();
  }

  default int validateMenuOption() throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getReader().readLine();
      stOption = stOption.trim();
      if (!ValidateInputs.validateInt(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  default Integer validateInt() throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateInt(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  default Integer validateInt(String title) throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateInt(stOption)) {
        printTitleInvalidInput(title);
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  default String validateString() throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateString(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = stOption;
    }
    return option;
  }

  default String validateString(String title) throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateString(stOption)) {
        printTitleInvalidInput(title);
        continue;
      }
      option = stOption;
    }
    return option;
  }

  default Double validateDouble() throws IOException {
    double option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateDouble(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = Double.parseDouble(stOption);
    }
    return option;
  }

  default Double validateDouble(String title) throws IOException {
    double option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateDouble(stOption)) {
        printTitleInvalidInput(title);
        continue;
      }
      option = Double.parseDouble(stOption);
    }
    return option;
  }

  default Boolean validateBoolean() throws IOException {
    boolean option = false;
    boolean output = false;
    while (!option) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateBoolean(stOption.toLowerCase())) {
        printComponentInvalidInput();
        continue;
      }
      output = Boolean.parseBoolean(stOption.toLowerCase());
      option = true;
    }
    return output;
  }

  default Boolean validateBoolean(String title) throws IOException {
    boolean option = false;
    boolean output = false;
    while (!option) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateBoolean(stOption.toLowerCase())) {
        printTitleInvalidInput(title);
        continue;
      }
      output = Boolean.parseBoolean(stOption.toLowerCase());
      option = true;
    }
    return output;
  }

  default String validateEmail() throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateEmail(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = stOption;
    }
    return option;
  }

  default String validateEmail(String title) throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateEmail(stOption)) {
        printTitleInvalidInput(title);
        continue;
      }
      option = stOption;
    }
    return option;
  }

  default String validateUuid() throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateUuid(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = stOption;
    }
    return option;
  }

  default String validateUuid(String title) throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateUuid(stOption)) {
        printTitleInvalidInput(title);
        continue;
      }
      option = stOption;
    }
    return option;
  }

  default String validateIsbn() throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateISBN(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = stOption;
    }
    return option;
  }

  default String validateIsbn(String title) throws IOException {
    String option = "";
    while (option.isEmpty()) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateISBN(stOption)) {
        printTitleInvalidInput(title);
        continue;
      }
      option = stOption;
    }
    return option;
  }

  default Integer validateYear() throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateYear(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  default Integer validateYear(String title) throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validateYear(stOption)) {
        printTitleInvalidInput(title);
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  default Integer validatePhoneNumber() throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validatePhoneNumber(stOption)) {
        printComponentInvalidInput();
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  default Integer validatePhoneNumber(String title) throws IOException {
    int option = -1;
    while (option < 1) {
      String stOption = getInput();
      if (stOption == null) {
        return null;
      }
      if (!ValidateInputs.validatePhoneNumber(stOption)) {
        printTitleInvalidInput(title);
        continue;
      }
      option = Integer.parseInt(stOption);
    }
    return option;
  }

  default Integer readInt(String title) throws IOException {
    getWriter().write(title + '\n');
    getWriter().flush();
    return validateInt(title);
  }

  default String readString(String title) throws IOException {
    getWriter().write(title + '\n');
    getWriter().flush();
    return validateString(title);
  }

  default Double readDouble(String title) throws IOException {
    getWriter().write(title + '\n');
    getWriter().flush();
    return validateDouble(title);
  }

  default Boolean readBoolean(String title) throws IOException {
    getWriter().write(title + '\n');
    getWriter().flush();
    return validateBoolean(title);
  }

  default String readEmail(String email) throws IOException {
    getWriter().write(email + '\n');
    getWriter().flush();
    return validateEmail(email);
  }

  default String readUuid(String uuid) throws IOException {
    getWriter().write(uuid + '\n');
    getWriter().flush();
    return validateUuid(uuid);
  }

  default String readIsbn(String isbn) throws IOException {
    getWriter().write(isbn + '\n');
    getWriter().flush();
    return validateIsbn(isbn);
  }

  default Integer readYear(String year) throws IOException {
    getWriter().write(year + '\n');
    getWriter().flush();
    return validateYear(year);
  }

  default Integer readPhoneNumber(String phoneNumber) throws IOException {
    getWriter().write(phoneNumber + '\n');
    getWriter().flush();
    return validatePhoneNumber(phoneNumber);
  }

  default <T extends Record & Comparable<T>> void printRecordList(RecordsTreeList<T> list) throws IOException {
    String output = list.toString();
    getWriter().write(output);
    getWriter().flush();
  }

  default <T extends Record> void printRecordList(RecordsList<T> list) throws IOException {
    String output = list.toString();
    getWriter().write(output);
    getWriter().flush();
  }

  default void write(String input) throws IOException {
    getWriter().write(input + "\n");
    getWriter().flush();
  }
}

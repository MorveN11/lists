package programming4.library.app.interfaces;

import java.io.IOException;

public interface Component extends TerminalComponent {

  void initComponent() throws IOException;

  default void printComponent() throws IOException {
    getWriter().write(this.toString());
    getWriter().flush();
  }

  default void exitApplication() throws IOException {
    getWriter().close();
    getReader().close();
    System.exit(0);
  }
}

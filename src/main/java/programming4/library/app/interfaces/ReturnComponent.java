package programming4.library.app.interfaces;

import java.io.IOException;

public interface ReturnComponent<T> extends OptionalComponent {

  @Override
  default void initComponent() {
  }

  T initReturnComponent() throws IOException;
}

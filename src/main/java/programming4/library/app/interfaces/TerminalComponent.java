package programming4.library.app.interfaces;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public interface TerminalComponent {

  BufferedReader getReader();

  BufferedWriter getWriter();
}

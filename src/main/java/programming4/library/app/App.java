package programming4.library.app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import programming4.library.app.components.login.LoginMenu;
import programming4.library.classes.library.Library;

public class App {

  private final LoginMenu loginMenu;

  private final BufferedWriter bw;

  private final BufferedReader br;

  public App(Library library) {
    this.bw = new BufferedWriter(new OutputStreamWriter(System.out));
    this.br = new BufferedReader(new InputStreamReader(System.in));
    this.loginMenu = new LoginMenu(library, br, bw);
  }

  public void initApp() throws IOException {
    loginMenu.initComponent();
    br.close();
    bw.close();
  }
}

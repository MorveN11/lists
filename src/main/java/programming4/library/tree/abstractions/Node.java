package programming4.library.tree.abstractions;

public class Node<T> {

  private T value;

  private Node<T> left;

  private Node<T> right;

  public Node(T value) {
    this.value = value;
    right = null;
    left = null;
  }

  public T getValue() {
    return value;
  }

  public void setValue(T value) {
    this.value = value;
  }

  public Node<T> getLeft() {
    return left;
  }

  public void setLeft(Node<T> left) {
    this.left = left;
  }

  public Node<T> getRight() {
    return right;
  }

  public void setRight(Node<T> right) {
    this.right = right;
  }

  @Override
  public String toString() {
    return "%s <- %s -> %s".formatted(
            left == null ? "null" : left.getValue().toString(),
            value.toString(),
            right == null ? "null" : right.getValue().toString()
    );
  }

  @Override
  public boolean equals(Object object) {
    if (object == null) {
      return false;
    }
    if (this.getClass() != object.getClass()) {
      return false;
    }
    Node<?> obj = (Node<?>) object;
    return this.value.equals(obj.getValue());
  }
}

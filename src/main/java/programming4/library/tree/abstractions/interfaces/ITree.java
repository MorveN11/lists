package programming4.library.tree.abstractions.interfaces;

public interface ITree<T> {

  String inOrder();

  T add(T value);

  T remove(T value);

  T get(T value);

  T update(T value, T updatedValue);
}

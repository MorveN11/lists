package programming4.library.tree.abstractions;

import programming4.library.tree.abstractions.interfaces.ITree;

public class Tree<T extends Comparable<T>> implements ITree<T> {

  private Node<T> root;

  public Tree() {
    root = null;
  }

  private T findRecursive(Node<T> current, T value) {
    if (current == null) {
      return null;
    }
    if (value.equals(current.getValue())) {
      return current.getValue();
    }
    return value.compareTo(current.getValue()) < 0
            ? findRecursive(current.getLeft(), value)
            : findRecursive(current.getRight(), value);
  }

  public Node<T> getRoot() {
    return root;
  }

  public void setRoot(Node<T> root) {
    this.root = root;
  }

  protected Node<T> removeNode(Node<T> current) {
    if (current.getLeft() == null) {
      return current.getRight();
    }
    if (current.getRight() == null) {
      return current.getLeft();
    }
    Node<T> succParent = current;
    Node<T> succ = current.getRight();
    while (succ.getLeft() != null) {
      succParent = succ;
      succ = succ.getLeft();
    }
    if (succParent != current) {
      succParent.setLeft(succ.getRight());
    } else {
      succParent.setRight(succ.getRight());
    }
    current.setValue(succ.getValue());
    return current;
  }

  private String inOrderRecursive(Node<T> current, StringBuilder st) {
    if (current == null) {
      return null;
    }
    inOrderRecursive(current.getLeft(), st);
    st.append(current.getValue()).append(" ");
    inOrderRecursive(current.getRight(), st);
    return st.toString().trim();
  }

  private Node<T> addRecursive(Node<T> current, T value) {
    if (current == null) {
      return new Node<>(value);
    }
    if (value.compareTo(current.getValue()) < 0) {
      current.setLeft(addRecursive(current.getLeft(), value));
    } else if (value.compareTo(current.getValue()) > 0) {
      current.setRight(addRecursive(current.getRight(), value));
    } else if (value.equals(current.getValue())) {
      return current;
    }
    return current;
  }

  private Node<T> removeRecursive(Node<T> current, T value) {
    if (current == null) {
      return null;
    }
    if (current.getValue().compareTo(value) > 0) {
      current.setLeft(removeRecursive(current.getLeft(), value));
      return current;
    } else if (current.getValue().compareTo(value) < 0) {
      current.setRight(removeRecursive(current.getRight(), value));
      return current;
    }
    return removeNode(current);
  }

  private T getRecursive(Node<T> current, T value) {
    if (current == null) {
      return null;
    }
    if (value.equals(current.getValue())) {
      return current.getValue();
    }
    return value.compareTo(current.getValue()) < 0
            ? getRecursive(current.getLeft(), value)
            : getRecursive(current.getRight(), value);
  }

  private T updateRecursive(Node<T> current, T value, T updatedValue) {
    if (current == null) {
      return null;
    }
    if (value.equals(current.getValue())) {
      current.setValue(updatedValue);
      return current.getValue();
    }
    return value.compareTo(current.getValue()) < 0
            ? updateRecursive(current.getLeft(), value, updatedValue)
            : updateRecursive(current.getRight(), value, updatedValue);
  }

  private T find(T value) {
    return findRecursive(root, value);
  }

  @Override
  public String inOrder() {
    StringBuilder st = new StringBuilder();
    return inOrderRecursive(root, st);
  }

  @Override
  public T add(T value) {
    T valueToAdd = find(value);
    if (valueToAdd != null) {
      return null;
    }
    root = addRecursive(root, value);
    return value;
  }

  @Override
  public T remove(T value) {
    T valueToRemove = find(value);
    if (valueToRemove == null) {
      return null;
    }
    root = removeRecursive(root, value);
    return value;
  }

  @Override
  public T get(T value) {
    return getRecursive(root, value);
  }

  @Override
  public T update(T value, T updatedValue) {
    return updateRecursive(root, value, updatedValue);
  }

  @Override
  public String toString() {
    return inOrder();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Tree<?> other)) {
      return false;
    }
    return this.inOrder().equals(other.inOrder());
  }
}

package programming4.library.tree.records.interfaces;

import java.util.UUID;

public interface IRecordsTree<T> {

  T removeById(String id);

  T getById(String id);

  T updateById(String id, T valueUpdated);

  T removeById(UUID id);

  T getById(UUID id);

  T updateById(UUID id, T valueUpdated);
}

package programming4.library.tree.records;

import java.util.UUID;
import programming4.library.classes.interfaces.Record;
import programming4.library.errors.NotExistsIdException;
import programming4.library.errors.NotUniqueIdException;
import programming4.library.tree.abstractions.Node;
import programming4.library.tree.abstractions.Tree;
import programming4.library.tree.records.interfaces.IRecordsTree;

public class RecordsTree<T extends Record & Comparable<T>> extends Tree<T> implements IRecordsTree<T> {

  public RecordsTree() {
    super();
  }

  private Node<T> removeByIdRecursive(Node<T> current, String id) {
    if (current == null) {
      return null;
    }
    if (current.getValue().getId().toString().compareTo(id) > 0) {
      current.setLeft(removeByIdRecursive(current.getLeft(), id));
      return current;
    } else if (current.getValue().getId().toString().compareTo(id) < 0) {
      current.setRight(removeByIdRecursive(current.getRight(), id));
      return current;
    }
    return removeNode(current);
  }

  private T getByIdRecursive(Node<T> current, String id) {
    if (current == null) {
      return null;
    }
    String currentId = current.getValue().getId().toString();
    if (id.equals(currentId)) {
      return current.getValue();
    }
    return id.compareTo(currentId) < 0
            ? getByIdRecursive(current.getLeft(), id)
            : getByIdRecursive(current.getRight(), id);
  }

  private T updateByIdRecursive(Node<T> current, String id, T valueUpdated) {
    if (current == null) {
      return null;
    }
    String currentId = current.getValue().getId().toString();
    if (id.equals(currentId)) {
      current.setValue(valueUpdated);
      return current.getValue();
    }
    return id.compareTo(currentId) < 0
            ? updateByIdRecursive(current.getLeft(), id, valueUpdated)
            : updateByIdRecursive(current.getRight(), id, valueUpdated);
  }

  @Override
  public T removeById(String id) {
    T valueToRemove = getById(id);
    if (valueToRemove == null) {
      return null;
    }
    setRoot(removeByIdRecursive(getRoot(), id));
    return valueToRemove;
  }

  @Override
  public T getById(String id) {
    T elementGot = getByIdRecursive(getRoot(), id);
    if (elementGot == null) {
      throw new NotExistsIdException();
    }
    return elementGot;
  }

  @Override
  public T updateById(String id, T valueUpdated) {
    T elementUpdated = updateByIdRecursive(getRoot(), id, valueUpdated);
    if (elementUpdated == null) {
      throw new NotExistsIdException();
    }
    return elementUpdated;
  }

  @Override
  public T removeById(UUID id) {
    return removeById(id.toString());
  }

  @Override
  public T getById(UUID id) {
    return getById(id.toString());
  }

  @Override
  public T updateById(UUID id, T valueUpdated) {
    return updateById(id.toString(), valueUpdated);
  }

  @Override
  public T add(T value) {
    T elementAdded = super.add(value);
    if (elementAdded == null) {
      throw new NotUniqueIdException();
    }
    return elementAdded;
  }

  @Override
  public T get(T value) {
    T elementGot = super.get(value);
    if (elementGot == null) {
      throw new NotExistsIdException();
    }
    return elementGot;
  }

  @Override
  public T remove(T value) {
    T elementRemoved = super.remove(value);
    if (elementRemoved == null) {
      throw new NotExistsIdException();
    }
    return elementRemoved;
  }

  @Override
  public T update(T value, T valueUpdated) {
    T elementUpdated = super.update(value, valueUpdated);
    if (elementUpdated == null) {
      throw new NotExistsIdException();
    }
    return elementUpdated;
  }
}

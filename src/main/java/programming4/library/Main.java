package programming4.library;

import java.io.IOException;
import programming4.library.app.App;
import programming4.library.classes.library.Library;
import programming4.library.classes.loans.Loan;
import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.treeList.loans.LoansTreeList;
import programming4.library.treeList.materials.MaterialsTreeList;
import programming4.library.treeList.persons.PersonsTreeList;

public class Main {

  public static void main(String[] args) throws IOException {
    PersonsTreeList persons = new PersonsTreeList();
    MaterialsTreeList materials = new MaterialsTreeList();
    LoansTreeList loans = new LoansTreeList();
    Library library = new Library();
    persons.add(new Student("05a8def5-2380-47a5-849a-06fe260ded95",
            "Jose", "Morales", "Av. Blanco Galindo",
            "morales.patty.jose@gmail.com", 75463290));
    persons.add(new Teacher("3d686983-ec44-42e1-8eea-e637ebc3f034",
            "Miguel", "Romero", "Av. America",
            "micky.romesa@gmail.com", 75423890));
    persons.add(new Student("1f6fd5a4-90c7-4bf3-b26d-eeb3dcbcaf84",
            "Santi", "Quiroga", "Av.Melchor Perez de Olguin",
            "santiago.quiroga@gmail.com", 73456209));
    materials.add(new Book("bd3ce7a4-1410-451b-8fb9-aeb9598ab322",
            "El nombre del viento", "Patrick Rothfuss", "Fantasía",
            "978-8401352836", 2007, 672, true,
            5, "Plaza & Janés", "Book"));
    materials.add(new Book("68236103-8985-4384-a910-080fb8a521dc",
            "1984", "George Orwell", "Ciencia Ficción",
            "978-0451524935", 1949, 328, true,
            2, "Signet Classics", "Book"));
    materials.add(new Book("ab23e81c-8fa6-40de-a19f-5831882f32e4",
            "El principito", "Antoine de Saint-Exupéry", "Fábula",
            "978-0156013925", 1943, 96, true,
            15, "Harcourt, Brace & World", "Book"));
    materials.add(new Book("b9a458a2-a329-44c0-8bbb-d10fb4d37941",
            "Cien años de soledad", "Gabriel García Márquez", "Realismo mágico",
            "978-0307474728", 1967, 417, true,
            20, "Vintage Espanol", "Book"));
    materials.add(new Magazine("f0c74ad3-ea61-4fa9-bca8-78a3913a0a71",
            "National Geographic", "National Geographic Society", "Ciencia y Naturaleza",
            "978-8482986425", 2021, 148, true,
            100, "Magazine"));
    materials.add(new Magazine("0da5d3cb-419e-46b9-adba-53b8e414b19b",
            "Time", "Time Magazine", "Actualidad",
            "978-1603209796", 2022, 96, true,
            -10, "Magazine"));
    materials.add(new Magazine("c286d43c-6d1a-4b32-9425-53327e74dd34",
            "Scientific American", "Springer Nature", "Ciencia",
            "978-0470712588", 2023, 80, true,
            300, "Magazine"));
    materials.add(new Magazine("8663bbc0-eb3c-44f4-a4c3-2e3a95e49608",
            "The New Yorker", "Condé Nast", "Cultura",
            "978-0000000000", 2023, 120, true,
            50, "Magazine"));
    loans.add(new Loan("2775ca57-fa61-4b00-af9a-d9ac6e76a6e7", materials.getList().getByIndex(0),
            persons.getList().getByIndex(0)));
    loans.add(new Loan("4c8ed66d-e3ba-4f17-b7a5-dacc23f599dd", materials.getList().getByIndex(1),
            persons.getList().getByIndex(1)));
    loans.add(new Loan("d4d5677b-4e06-434f-b2c3-a6ed933eee4e", materials.getList().getByIndex(2),
            persons.getList().getByIndex(2)));
    loans.add(new Loan("ab6b2996-eee9-4699-9684-a513958bc444", materials.getList().getByIndex(3),
            persons.getList().getByIndex(1)));
    loans.add(new Loan("5443592d-1ce6-4509-949f-159e0a9efe3c", materials.getList().getByIndex(4),
            persons.getList().getByIndex(0)));
    library.loadMaterials(materials);
    library.loadPersons(persons);
    library.loadLoans(loans);
    App app = new App(library);
    app.initApp();
  }
}

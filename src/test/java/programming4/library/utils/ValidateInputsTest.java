package programming4.library.utils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ValidateInputsTest {

  private static final String[] VALID_PHONE_NUMBERS = {
          "12345677",
          "98765437",
          "65432109"
  };

  private static final String[] INVALID_PHONE_NUMBERS = {
          "+5911234567",
          "123456",
          "abcdefgh",
          "7654321",
          "98765432a"
  };

  @Test
  public void testValidateInt_ValidInput() {
    assertTrue(ValidateInputs.validateInt("123"));
    assertTrue(ValidateInputs.validateInt("-456"));
  }

  @Test
  public void testValidateInt_InvalidInput() {
    assertFalse(ValidateInputs.validateInt("abc"));
    assertFalse(ValidateInputs.validateInt("12.3"));
    assertFalse(ValidateInputs.validateInt(""));
  }

  @Test
  public void testValidateDouble_ValidInput() {
    assertTrue(ValidateInputs.validateDouble("123.45"));
    assertTrue(ValidateInputs.validateDouble("-456.78"));
    assertTrue(ValidateInputs.validateDouble("789"));
  }

  @Test
  public void testValidateDouble_InvalidInput() {
    assertFalse(ValidateInputs.validateDouble("abc"));
    assertFalse(ValidateInputs.validateDouble("12.3.45"));
    assertFalse(ValidateInputs.validateDouble(""));
  }

  @Test
  public void testValidateString_ValidInput() {
    assertTrue(ValidateInputs.validateString("Hello"));
    assertTrue(ValidateInputs.validateString("World"));
  }

  @Test
  public void testValidateString_InvalidInput() {
    assertTrue(ValidateInputs.validateString("123"));
    assertFalse(ValidateInputs.validateString(""));
  }

  @Test
  public void testValidateBoolean_ValidInput() {
    assertTrue(ValidateInputs.validateBoolean("true"));
    assertTrue(ValidateInputs.validateBoolean("false"));
    assertTrue(ValidateInputs.validateBoolean("TRUE"));
    assertTrue(ValidateInputs.validateBoolean("FALSE"));
  }

  @Test
  public void testValidateBoolean_InvalidInput() {
    assertFalse(ValidateInputs.validateBoolean("abc"));
    assertFalse(ValidateInputs.validateBoolean("123"));
    assertFalse(ValidateInputs.validateBoolean(""));
  }

  @Test
  public void testValidateNotEmpty_ValidInput() {
    assertTrue(ValidateInputs.validateNotEmpty("Hello"));
    assertTrue(ValidateInputs.validateNotEmpty("123"));
    assertTrue(ValidateInputs.validateNotEmpty("true"));
  }

  @Test
  public void testValidateNotEmpty_InvalidInput() {
    assertFalse(ValidateInputs.validateNotEmpty(""));
  }

  @Test
  public void testValidateEmail_ValidInput() {
    assertTrue(ValidateInputs.validateEmail("john.doe@example.com"));
    assertTrue(ValidateInputs.validateEmail("alice123@example.org"));
    assertTrue(ValidateInputs.validateEmail("info@example.co.uk"));
  }

  @Test
  public void testValidateEmail_InvalidInput() {
    assertFalse(ValidateInputs.validateEmail("notAnEmail"));
    assertFalse(ValidateInputs.validateEmail("@example.com"));
    assertFalse(ValidateInputs.validateEmail("user@.com"));
    assertFalse(ValidateInputs.validateEmail("user@example"));
    assertFalse(ValidateInputs.validateEmail("user@example.c"));
  }

  @Test
  public void testValidateUuid_ValidInput() {
    assertTrue(ValidateInputs.validateUuid("123e4567-e89b-12d3-a456-426614174000"));
    assertTrue(ValidateInputs.validateUuid("123e4567-e89b-12d3-a456-426614174001"));
    assertTrue(ValidateInputs.validateUuid("05364ca0-6947-430d-85a4-711ba104af26"));
  }

  @Test
  public void testValidateUuid_InvalidInput() {
    assertFalse(ValidateInputs.validateUuid("123e4567-e89b-a456-426614174000"));
    assertFalse(ValidateInputs.validateUuid("123e4567-e89b-12d3-a456-"));
    assertFalse(ValidateInputs.validateUuid("123e4567-e89b-12d3--426614174002"));
  }

  @Test
  public void testValidateISBN_ValidInput() {
    assertTrue(ValidateInputs.validateISBN("123456789X"));
    assertTrue(ValidateInputs.validateISBN("0-471-72456-X"));
    assertTrue(ValidateInputs.validateISBN("9780470059029"));
    assertTrue(ValidateInputs.validateISBN("978-0-13-149505-0"));
  }

  @Test
  public void testValidateISBN_InvalidInput() {
    assertFalse(ValidateInputs.validateISBN("12345"));
    assertFalse(ValidateInputs.validateISBN("12345678901234"));
    assertFalse(ValidateInputs.validateISBN("978-0-13-149505-012"));
    assertFalse(ValidateInputs.validateISBN("abc"));
  }

  @Test
  public void testValidateYear_ValidInput() {
    assertTrue(ValidateInputs.validateYear("1999"));
    assertTrue(ValidateInputs.validateYear("2021"));
    assertTrue(ValidateInputs.validateYear("1900"));
    assertTrue(ValidateInputs.validateYear("2099"));
  }

  @Test
  public void testValidateYear_InvalidInput() {
    assertFalse(ValidateInputs.validateYear("99"));
    assertFalse(ValidateInputs.validateYear("20021"));
    assertFalse(ValidateInputs.validateYear("abcd"));
    assertFalse(ValidateInputs.validateYear("1899"));
    assertFalse(ValidateInputs.validateYear("2100"));
  }

  @Test
  public void testValidateSimilarString_ValidInput() {
    assertFalse(ValidateInputs.similarString("hello", "cielo"));
    assertTrue(ValidateInputs.similarString("hello", "holo"));
    assertTrue(ValidateInputs.similarString("Hello", "Hello"));
    assertTrue(ValidateInputs.similarString("Hello", " Hello "));
  }

  @Test
  public void testValidateSimilarString_InvalidInput() {
    assertFalse(ValidateInputs.similarString("Alan", "Luis"));
    assertFalse(ValidateInputs.similarString("Hello", "World"));
    assertFalse(ValidateInputs.similarString("", "Hello"));
    assertFalse(ValidateInputs.similarString("Hello", ""));
    assertTrue(ValidateInputs.similarString("Hello", "Hello "));
    assertTrue(ValidateInputs.similarString("Hello", " Hello"));
    assertTrue(ValidateInputs.similarString("Hello", "Healo"));
  }

  @Test
  public void testValidPhoneNumbers() {
    for (String phoneNumber : VALID_PHONE_NUMBERS) {
      assertTrue(ValidateInputs.validatePhoneNumber(phoneNumber),
              "Expected " + phoneNumber + " to be a valid phone number");
    }
  }

  @Test
  public void testInvalidPhoneNumbers() {
    for (String phoneNumber : INVALID_PHONE_NUMBERS) {
      assertFalse(ValidateInputs.validatePhoneNumber(phoneNumber),
              "Expected " + phoneNumber + " to be an invalid phone number");
    }
  }
}

package programming4.library.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ConstantsTest {

  @Test
  void testConstants() {
    assertEquals(0, Constants.DEFAULT_DEBT);
    assertEquals(0, Constants.DEFAULT_NUM_BOOKS);
    assertEquals(1, Constants.DEFAULT_NUM_LOAN_MONTHS);
    assertEquals("--------------------------", Constants.LINE);
    assertEquals(Constants.LINE.substring(0, Constants.LINE.length() / 2), Constants.HALF_LINE);
    assertEquals("Invalid input. Please try again.", Constants.INVALID_INPUT);
  }
}

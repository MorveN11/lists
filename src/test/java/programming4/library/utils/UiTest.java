package programming4.library.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class UiTest {

  @Test
  void printTitle() {
    assertEquals("%s Title %s".formatted(Constants.HALF_LINE, Constants.HALF_LINE), Ui.printTitle("Title"));
  }

  @Test
  void printLine() {
    assertEquals(Constants.LINE, Ui.printLine());
  }

  @Test
  void invalidInput() {
    assertEquals(Constants.INVALID_INPUT, Ui.invalidInput());
  }
}

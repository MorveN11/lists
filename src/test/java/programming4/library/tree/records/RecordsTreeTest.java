package programming4.library.tree.records;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.errors.NotExistsIdException;
import programming4.library.errors.NotUniqueIdException;

class RecordsTreeTest {

  private RecordsTree<Person> recordsTree;

  @BeforeEach
  void setUp() {
    recordsTree = new RecordsTree<>();
    Person student1 = new Student("05a8def5-2380-47a5-849a-06fe260ded95", "John", "Doe", "Address1",
            "john.doe@example.com", 123456789);
    Person student2 = new Student("bf9818ea-8eb9-4c19-aa4f-118f54391e39", "Jane", "Smith", "Address2",
            "jane.smith@example.com", 987654321);
    recordsTree.add(student1);
    recordsTree.add(student2);
  }

  @Test
  void removeById() {
    System.out.println(recordsTree);
    String idToRemove = "05a8def5-2380-47a5-849a-06fe260ded95";
    Person removedPerson = recordsTree.removeById(idToRemove);
    assertNotNull(removedPerson);
    assertEquals(idToRemove, removedPerson.getId().toString());
    assertThrows(NotExistsIdException.class, () -> recordsTree.getById(idToRemove));
  }

  @Test
  void removeById_NotExists() {
    String idToRemove = "nonexistent-id";
    assertThrows(NotExistsIdException.class, () -> recordsTree.removeById(idToRemove));
  }

  @Test
  void getById() {
    String idToGet = "bf9818ea-8eb9-4c19-aa4f-118f54391e39";
    System.out.println(recordsTree);
    Person person = recordsTree.getById(idToGet);
    assertNotNull(person);
    assertEquals(idToGet, person.getId().toString());
  }

  @Test
  void getById_NotExists() {
    String idToGet = "nonexistent-id";
    assertThrows(NotExistsIdException.class, () -> recordsTree.getById(idToGet));
  }

  @Test
  void updateById() {
    String idToUpdate = "bf9818ea-8eb9-4c19-aa4f-118f54391e39";
    Person newPersonData = new Student(idToUpdate, "Updated", "Student", "Updated Address",
            "updated@student.com", 987654321);
    Person updatedPerson = recordsTree.updateById(idToUpdate, newPersonData);
    assertNotNull(updatedPerson);
    assertEquals(idToUpdate, updatedPerson.getId().toString());
    assertEquals("Updated", updatedPerson.getName());
    assertEquals("Student", updatedPerson.getLastname());
    assertEquals("Updated Address", updatedPerson.getAddress());
    assertEquals("updated@student.com", updatedPerson.getEmail());
    assertEquals(987654321, updatedPerson.getPhoneNumber());
  }

  @Test
  void updateById_NotExists() {
    String idToUpdate = "a0bb8cbc-89cb-4361-9439-ed22283512d6";
    Person newStudentData = new Student(idToUpdate, "Updated", "Student", "Updated Address",
            "updated@student.com", 987654321);
    assertThrows(NotExistsIdException.class, () -> recordsTree.updateById(idToUpdate, newStudentData));
  }

  @Test
  void add() {
    String newStudentId = "4e23b0ee-2f47-42ea-9ec4-34e4a2684d72";
    Person newPerson = new Student(newStudentId, "New", "Student", "New Address",
            "new@student.com", 987654321);
    Person addedPerson = recordsTree.add(newPerson);
    assertNotNull(addedPerson);
    assertEquals(newStudentId, addedPerson.getId().toString());
    assertEquals("New", addedPerson.getName());
    assertEquals("Student", addedPerson.getLastname());
    assertEquals("New Address", addedPerson.getAddress());
    assertEquals("new@student.com", addedPerson.getEmail());
    assertEquals(987654321, addedPerson.getPhoneNumber());
  }

  @Test
  void add_DuplicateId() {
    String existingId = "05a8def5-2380-47a5-849a-06fe260ded95";
    Person personWithExistingId = new Student(existingId, "Duplicate", "Student", "Duplicate Address",
            "duplicate@student.com", 123456789);
    assertThrows(NotUniqueIdException.class, () -> recordsTree.add(personWithExistingId));
  }

  @Test
  void remove() {
    Person personToRemove = new Student("bf9818ea-8eb9-4c19-aa4f-118f54391e39", "Jane", "Smith", "Address2",
            "jane.smith@example.com", 987654321);
    Person removedPerson = recordsTree.remove(personToRemove);
    assertNotNull(removedPerson);
    assertEquals(personToRemove.getId(), removedPerson.getId());
    assertThrows(NotExistsIdException.class, () -> recordsTree.getById(personToRemove.getId().toString()));
  }

  @Test
  void remove_NotExists() {
    Person studentToRemove = new Student("9d022b07-7612-4a29-a7ad-812c5259ef75", "Nonexistent", "Student", "Nonexistent Address",
            "nonexistent@student.com", 123456789);
    assertThrows(NotExistsIdException.class, () -> recordsTree.remove(studentToRemove));
  }

  @Test
  void get() {
    String idToGet = "bf9818ea-8eb9-4c19-aa4f-118f54391e39";
    Person student = new Student(idToGet, "Jane", "Smith", "Address2",
            "jane.smith@example.com", 987654321);
    Person studentGot = recordsTree.get(student);
    assertNotNull(studentGot);
    assertEquals(idToGet, studentGot.getId().toString());
  }

  @Test
  void get_NotExists() {
    Person studentToGet = new Student("57bfbeb0-f6bf-4786-82f6-be15c8aceeca", "Nonexistent", "Student", "Nonexistent Address",
            "nonexistent@student.com", 123456789);
    assertThrows(NotExistsIdException.class, () -> recordsTree.get(studentToGet));
  }

  @Test
  void update() {
    Person studentToUpdate = new Student("bf9818ea-8eb9-4c19-aa4f-118f54391e39", "Jane", "Smith", "Address2",
            "jane.smith@example.com", 987654321);
    Person newStudentData = new Student("bf9818ea-8eb9-4c19-aa4f-118f54391e39", "Updated", "Student", "Updated Address",
            "updated@student.com", 987654321);
    Person updatedStudent = recordsTree.update(studentToUpdate, newStudentData);
    assertNotNull(updatedStudent);
    assertEquals(studentToUpdate.getId(), updatedStudent.getId());
    assertEquals("Updated", updatedStudent.getName());
    assertEquals("Student", updatedStudent.getLastname());
    assertEquals("Updated Address", updatedStudent.getAddress());
    assertEquals("updated@student.com", updatedStudent.getEmail());
    assertEquals(987654321, updatedStudent.getPhoneNumber());
  }

  @Test
  void update_NotExists() {
    Person studentToUpdate = new Student("78850d18-87cc-4224-b4c0-fe83c50edca3", "Nonexistent", "Student", "Nonexistent Address",
            "nonexistent@student.com", 123456789);
    Person newStudentData = new Student("5f7d803b-3067-44a3-bc2a-2b3ca10e9300", "Updated", "Student", "Updated Address",
            "updated@student.com", 987654321);
    assertThrows(NotExistsIdException.class, () -> recordsTree.update(studentToUpdate, newStudentData));
  }

  @Test
  void add_DuplicateIdInTree() {
    Person existingStudent = new Student("05a8def5-2380-47a5-849a-06fe260ded95", "John", "Doe", "Address1",
            "john.doe@example.com", 123456789);
    assertThrows(NotUniqueIdException.class, () -> recordsTree.add(existingStudent));
  }

  @Test
  void remove_NotExistsInTree() {
    Person nonexistentStudent = new Student("10b4cc4b-719e-4009-a0c8-476b54ad2a95", "Nonexistent", "Student", "Nonexistent Address",
            "nonexistent@student.com", 123456789);
    assertThrows(NotExistsIdException.class, () -> recordsTree.remove(nonexistentStudent));
  }

  @Test
  void get_NotExistsInTree() {
    Person nonexistentStudent = new Student("e28fb883-cc54-4f08-8da7-c0d0bc0fafa0", "Nonexistent", "Student", "Nonexistent Address",
            "nonexistent@student.com", 123456789);
    assertThrows(NotExistsIdException.class, () -> recordsTree.get(nonexistentStudent));
  }

  @Test
  void update_NotExistsInTree() {
    Person nonexistentStudent = new Student("cbdbe0a5-694f-4996-b2c9-aa4513bb623e", "Nonexistent", "Student", "Nonexistent Address",
            "nonexistent@student.com", 123456789);
    Person newStudentData = new Student("8ead288a-ea45-4019-92c5-23d8aca15e9b", "Updated", "Student", "Updated Address",
            "updated@student.com", 987654321);
    assertThrows(NotExistsIdException.class, () -> recordsTree.update(nonexistentStudent, newStudentData));
  }
}

package programming4.library.tree.abstractions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class NodeTest {

  private Node<String> node1;

  private Node<String> node2;

  @BeforeEach
  void setUp() {
    node1 = new Node<>("Value1");
    node2 = new Node<>("Value2");
  }

  @Test
  void getValue() {
    assertEquals("Value1", node1.getValue());
    assertEquals("Value2", node2.getValue());
  }

  @Test
  void setValue() {
    node1.setValue("NewValue1");
    assertEquals("NewValue1", node1.getValue());
  }

  @Test
  void getLeft() {
    assertNull(node1.getLeft());
    assertNull(node2.getLeft());
  }

  @Test
  void setLeft() {
    Node<String> leftNode = new Node<>("LeftValue");
    node1.setLeft(leftNode);
    assertEquals(leftNode, node1.getLeft());
  }

  @Test
  void getRight() {
    assertNull(node1.getRight());
    assertNull(node2.getRight());
  }

  @Test
  void setRight() {
    Node<String> rightNode = new Node<>("RightValue");
    node1.setRight(rightNode);
    assertEquals(rightNode, node1.getRight());
  }

  @Test
  void testToString() {
    node1.setLeft(new Node<>("LeftValue"));
    node1.setRight(new Node<>("RightValue"));
    assertEquals("LeftValue <- Value1 -> RightValue", node1.toString());
  }

  @Test
  void testEquals() {
    Node<String> node3 = new Node<>("Value1");
    Node<String> node4 = new Node<>("Value2");
    assertNotEquals(node1, node2);
    assertEquals(node1, node3);
    assertEquals(node2, node4);
  }
}

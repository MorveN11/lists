package programming4.library.tree.abstractions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TreeTest {

  private Tree<String> tree;

  @BeforeEach
  void setUp() {
    tree = new Tree<>();
    tree.add("apple");
    tree.add("banana");
    tree.add("cherry");
    tree.add("date");
  }

  @Test
  void testGetRoot() {
    Node<String> root = tree.getRoot();
    assertNotNull(root);
    assertEquals("apple", root.getValue());
  }

  @Test
  void testSetRoot() {
    Node<String> newRoot = new Node<>("pear");
    tree.setRoot(newRoot);
    assertEquals(newRoot, tree.getRoot());
  }

  @Test
  void testAdd() {
    tree.add("grape");
    assertEquals("apple banana cherry date grape", tree.inOrder());
    assertNull(tree.add("banana"));
    assertEquals("apple banana cherry date grape", tree.inOrder());
  }

  @Test
  void testRemove() {
    assertEquals("apple banana cherry date", tree.inOrder());
    tree.remove("cherry");
    assertEquals("apple banana date", tree.inOrder());
    assertNull(tree.remove("cherry"));
  }

  @Test
  void testGet() {
    assertEquals("banana", tree.get("banana"));
    assertNull(tree.get("mango"));
  }

  @Test
  void testUpdate() {
    String updatedValue = tree.update("banana", "mango");
    assertEquals("mango", updatedValue);
    assertEquals("apple mango cherry date", tree.inOrder());
    assertNull(tree.update("banana", "Kiwi"));
  }

  @Test
  void testInOrder() {
    assertEquals("apple banana cherry date", tree.inOrder());
  }

  @Test
  void testToString() {
    assertEquals("apple banana cherry date", tree.toString());
  }

  @Test
  void testEquals() {
    Tree<String> tree2 = new Tree<>();
    tree2.add("apple");
    tree2.add("banana");
    tree2.add("cherry");
    tree2.add("date");
    assertEquals(tree, tree2);
  }
}

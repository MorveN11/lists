package programming4.library.list.records;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.errors.NotFoundException;

class RecordsListTest {

  private static RecordsList<Person> persons;

  private static RecordsList<Material> materials;

  @BeforeEach
  void setUp() {
    persons = new RecordsList<>();
    materials = new RecordsList<>();
    persons.add(new Student("e", "f", "g", "h", 5555555));
    persons.add(new Teacher("e", "f", "g", "h", 5555555));
    persons.add(new Student("e", "f", "g", "h", 5555555));
    persons.add(new Teacher("e", "f", "g", "h", 5555555));
    persons.add(new Student("e", "f", "g", "h", 5555555));
    persons.add(new Teacher("e", "f", "g", "h", 5555555));
    materials.add(new Book("El nombre del viento", "Patrick Rothfuss", "Fantasía",
            "978-8401352836", 2007, 672, true, 5, "Plaza & Janés", "Book"));
    materials.add(new Magazine("National Geographic", "National Geographic Society", "Ciencia y Naturaleza",
            "978-8482986425", 2021, 148, true, 100, "Magazine"));
    materials.add(new Book("1984", "George Orwell", "Ciencia Ficción",
            "978-0451524935", 1949, 328, true, 10, "Signet Classics", "Book"));
    materials.add(new Magazine("Time", "Time Magazine", "Actualidad",
            "978-1603209796", 2022, 96, true, 200, "Magazine"));
    materials.add(new Book("El principito", "Antoine de Saint-Exupéry", "Fábula",
            "978-0156013925", 1943, 96, true, 15, "Harcourt, Brace & World", "Book"));
    materials.add(new Magazine("Scientific American", "Springer Nature", "Ciencia",
            "978-0470712588", 2023, 80, true, 300, "Magazine"));
  }

  @Test
  void getById() {
    assertThrows(NotFoundException.class, () -> persons.getById("35235-235325-235253"));
    assertThrows(NotFoundException.class, () -> materials.getById("35235-235325-235253"));
    Person person = persons.getByIndex(0);
    Person equalPerson = persons.getById(person.getId());
    Person equalStringPerson = persons.getById(person.getId().toString());
    assertEquals(person, equalPerson);
    assertEquals(person, equalStringPerson);
    Material material = materials.getByIndex(0);
    Material equalMaterial = materials.getById(material.getId());
    Material equalMaterialString = materials.getById(material.getId().toString());
    assertEquals(material, equalMaterial);
    assertEquals(material, equalMaterialString);
  }

  @Test
  void updateById() {
    assertThrows(NotFoundException.class, () -> persons.updateById("35235-235325-235253", new Student("e", "f", "g", "h", 5555555)));
    assertThrows(NotFoundException.class, () -> materials.updateById("35235-235325-235253",
            new Magazine("Scientific American", "Springer Nature", "Ciencia",
                    "978-0470712588", 2023, 80, true, 300, "Book")));
    Person person1 = persons.getByIndex(0);
    Person person2 = persons.getByIndex(1);
    Person person1Update = new Student("e", "f", "g", "h", 5555555);
    Person person2Update = new Teacher("e", "f", "g", "h", 5555555);
    assertTrue(persons.updateById(person1.getId(), person1Update));
    assertTrue(persons.updateById(person2.getId().toString(), person2Update));
    assertEquals(person1Update, persons.getByIndex(0));
    assertEquals(person2Update, persons.getByIndex(1));
    Material material1 = materials.getByIndex(0);
    Material material2 = materials.getByIndex(1);
    Material material1Update = new Book("Cien años de soledad", "Gabriel García Márquez", "Realismo mágico",
            "978-0307474728", 1967, 417, true, 20, "Vintage Espanol", "Book");
    Material material2Update = new Magazine("Time", "Time Magazine", "Actualidad",
            "978-1603209796", 2022, 96, true, 200, "Magazine");
    assertTrue(materials.updateById(material1.getId(), material1Update));
    assertTrue(materials.updateById(material2.getId().toString(), material2Update));
    assertEquals(material1Update, materials.getByIndex(0));
    assertEquals(material2Update, materials.getByIndex(1));
  }

  @Test
  void removeById() {
    assertThrows(NotFoundException.class, () -> persons.removeById("35235-235325-235253"));
    assertThrows(NotFoundException.class, () -> materials.removeById("35235-235325-235253"));
    Person person1 = persons.getByIndex(0);
    Person person2 = persons.getByIndex(1);
    Person person3 = persons.getByIndex(2);
    assertTrue(persons.removeById(person2.getId().toString()));
    assertTrue(persons.removeById(person1.getId()));
    assertEquals(person3, persons.getByIndex(0));
    Material material1 = materials.getByIndex(0);
    Material material2 = materials.getByIndex(1);
    Material material3 = materials.getByIndex(2);
    assertTrue(materials.removeById(material2.getId()));
    assertTrue(materials.removeById(material1.getId().toString()));
    assertEquals(material3, materials.getByIndex(0));
  }
}

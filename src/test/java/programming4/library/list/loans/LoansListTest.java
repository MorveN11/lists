package programming4.library.list.loans;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.loans.Loan;
import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;

class LoansListTest {

  private LoansList loansList;

  @BeforeEach
  void setUp() {
    Person person = new Student("05a8def5-2380-47a5-849a-06fe260ded95",
            "Jose", "Morales", "Av. Blanco Galindo",
            "morales.patty.jose@gmail.com", 75463290);
    Person person2 = new Teacher("3d686983-ec44-42e1-8eea-e637ebc3f034",
            "Miguel", "Romero", "Av. America",
            "micky.romesa@gmail.com", 75423890);
    Material material = new Book("bd3ce7a4-1410-451b-8fb9-aeb9598ab322",
            "El nombre del viento", "Patrick Rothfuss", "Fantasía",
            "978-8401352836", 2007, 672, true,
            5, "Plaza & Janés", "Book");
    Material material2 = new Book("68236103-8985-4384-a910-080fb8a521dc",
            "1984", "George Orwell", "Ciencia Ficción",
            "978-0451524935", 1949, 328, true,
            10, "Signet Classics", "Book");
    Material material3 = new Book("ab23e81c-8fa6-40de-a19f-5831882f32e4",
            "El principito", "Antoine de Saint-Exupéry", "Fábula",
            "978-0156013925", 1943, 96, true,
            15, "Harcourt, Brace & World", "Book");
    Material material4 = new Book("b9a458a2-a329-44c0-8bbb-d10fb4d37941",
            "Cien años de soledad", "Gabriel García Márquez", "Realismo mágico",
            "978-0307474728", 1967, 417, true,
            20, "Vintage Espanol", "Book");
    Material material5 = new Magazine("f0c74ad3-ea61-4fa9-bca8-78a3913a0a71",
            "National Geographic", "National Geographic Society", "Ciencia y Naturaleza",
            "978-8482986425", 2021, 148, true,
            100, "Magazine");
    LocalDate date = LocalDate.of(2023, 6, 1);
    LocalDate date2 = LocalDate.of(2023, 7, 1);
    LocalDate returnData = LocalDate.of(2023, 7, 1);
    LocalDate returnData2 = LocalDate.of(2023, 8, 1);
    Loan loan = new Loan("53669b61-a984-4800-89d9-bec13dad2cd9", material, person);
    Loan loan2 = new Loan("c2ea4b79-ea88-46e0-97de-77fd97ba4324", material2, person2);
    Loan loan3 = new Loan("91ffe291-6666-4a78-a13f-44808a8bf9e6", material3, person);
    Loan loan4 = new Loan("e160391f-e9a7-40db-bf43-50060b91f802", material4, person2);
    Loan loan5 = new Loan("46ecd98d-c9ed-4255-b8a4-cb080b69c852", material5, person);
    Loan loan6 = new Loan(material2, person2, date, returnData);
    Loan loan7 = new Loan(material, person, date2, returnData2);
    Loan loan8 = new Loan(material4, person2, date, returnData);
    Loan loan9 = new Loan(material5, person, date2, returnData2);
    loansList = new LoansList();
    loansList.add(loan);
    loansList.add(loan2);
    loansList.add(loan3);
    loansList.add(loan4);
    loansList.add(loan5);
    loansList.add(loan6);
    loansList.add(loan7);
    loansList.add(loan8);
    loansList.add(loan9);
  }

  @Test
  void filterByBookId() {
    String bookId = "bd3ce7a4-1410-451b-8fb9-aeb9598ab322";
    LoansList filteredLoans = loansList.filterByBookId(bookId);
    assertNotNull(filteredLoans);
    assertEquals(2, filteredLoans.size());
  }

  @Test
  void filterByPersonId() {
    String personId = "05a8def5-2380-47a5-849a-06fe260ded95";
    LoansList filteredLoans = loansList.filterByPersonId(personId);
    assertNotNull(filteredLoans);
    assertEquals(5, filteredLoans.size());
  }

  @Test
  void filterByLoanDate() {
    LocalDate startDate = LocalDate.of(2023, 6, 1);
    LoansList filteredLoans = loansList.filterByLoanDate(startDate);
    assertNotNull(filteredLoans);
    assertEquals(2, filteredLoans.size());
  }

  @Test
  void filterByReturnDate() {
    LocalDate returnDate = LocalDate.of(2023, 7, 1);
    LoansList filteredLoans = loansList.filterByReturnDate(returnDate);
    assertNotNull(filteredLoans);
    assertEquals(2, filteredLoans.size());
  }
}

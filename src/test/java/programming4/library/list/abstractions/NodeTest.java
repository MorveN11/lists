package programming4.library.list.abstractions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class NodeTest {

  private static Node<Number> node;

  @BeforeEach
  void setUp() {
    node = new Node<>(5);
    node.setNext(new Node<>(10));
  }

  @Test
  void getValue() {
    assertEquals(5, node.getValue());
  }

  @Test
  void setValue() {
    assertEquals(5, node.getValue());
    node.setValue(10);
    assertEquals(10, node.getValue());
  }

  @Test
  void getNext() {
    assertEquals(10, node.getNext().getValue());
  }

  @Test
  void setNext() {
    assertEquals(10, node.getNext().getValue());
    node.setNext(new Node<>(15));
    assertEquals(15, node.getNext().getValue());
  }

  @Test
  void testToString() {
    assertEquals("5 -> 10", node.toString());
  }

  @Test
  void testEquals() {
    Node<Number> newNode = new Node<>(5);
    newNode.setNext(new Node<>(10));
    assertEquals(newNode, node);
    newNode.setNext(new Node<>(20));
    assertNotEquals(newNode, node);
  }
}

package programming4.library.list.abstractions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ListTest {

  private static List<Number> list;

  @BeforeEach
  void setUp() {
    list = new List<>();
    list.add(5);
    list.add(10);
    list.add(15);
    list.add(20);
  }

  @Test
  void getHeadNode() {
    assertEquals(5, list.getHeadNode().getValue());
    list.removeAtIndex(0);
    assertEquals(10, list.getHeadNode().getValue());
  }

  @Test
  void setHeadNode() {
    assertEquals(5, list.getHeadNode().getValue());
    Node<Number> newHead = new Node<>(10);
    newHead.setNext(list.getHeadNode().getNext());
    list.setHeadNode(newHead);
    assertEquals(10, list.getHeadNode().getValue());
  }

  @Test
  void contains() {
    assertTrue(list.contains(5));
    assertTrue(list.contains(10));
    assertFalse(list.contains(12));
    list.removeAtIndex(0);
    assertFalse(list.contains(5));
  }

  @Test
  void size() {
    assertEquals(4, list.size());
    list.removeAtIndex(0);
    assertEquals(3, list.size());
    list.removeAtIndex(2);
    assertEquals(2, list.size());
  }

  @Test
  void isEmpty() {
    assertFalse(list.isEmpty());
    list.removeAtIndex(0);
    list.removeAtIndex(0);
    list.removeAtIndex(0);
    list.removeAtIndex(0);
    assertTrue(list.isEmpty());
  }

  @Test
  void add() {
    Number addedItem = list.add(30);
    Number addedItem2 = list.add(35);
    assertEquals(30, addedItem);
    assertEquals(35, addedItem2);
    assertEquals(6, list.size());
  }

  @Test
  void addAtIndex() {
    assertThrows(IndexOutOfBoundsException.class, () -> list.addAtIndex(-1, 0));
    assertThrows(IndexOutOfBoundsException.class, () -> list.addAtIndex(4, 0));
    list.addAtIndex(0, -20);
    assertEquals(list.getByIndex(0), -20);
    list.addAtIndex(2, -25);
    assertEquals(list.getByIndex(2), -25);
  }

  @Test
  void addAll() {
    List<Number> list2 = new List<>();
    list2.add(25);
    list2.add(30);
    list2.add(35);
    assertTrue(list.addAll(list2));
    assertEquals(7, list.size());
    assertEquals(25, list.getByIndex(4));
    assertEquals(30, list.getByIndex(5));
    assertEquals(35, list.getByIndex(6));
  }

  @Test
  void getByIndex() {
    assertThrows(IndexOutOfBoundsException.class, () -> list.getByIndex(-1));
    assertThrows(IndexOutOfBoundsException.class, () -> list.getByIndex(4));
    assertThrows(IndexOutOfBoundsException.class, () -> list.addAtIndex(-1, 0));
    assertThrows(IndexOutOfBoundsException.class, () -> list.addAtIndex(4, 0));
    assertEquals(5, list.getByIndex(0));
    assertEquals(10, list.getByIndex(1));
    list.removeAtIndex(0);
    list.removeAtIndex(1);
    assertEquals(10, list.getByIndex(0));
    assertEquals(20, list.getByIndex(1));
  }

  @Test
  void updateByIndex() {
    assertThrows(IndexOutOfBoundsException.class, () -> list.updateByIndex(-1, 0));
    assertThrows(IndexOutOfBoundsException.class, () -> list.updateByIndex(4, 0));
    Number updatedIndex = list.updateByIndex(0, 100);
    assertEquals(updatedIndex, 100);
    assertEquals(100, list.getByIndex(0));
    list.updateByIndex(1, 800);
    assertEquals(800, list.getByIndex(1));
  }

  @Test
  void removeAtIndex() {
    assertThrows(IndexOutOfBoundsException.class, () -> list.removeAtIndex(-1));
    assertThrows(IndexOutOfBoundsException.class, () -> list.removeAtIndex(4));
    assertEquals(5, list.getByIndex(0));
    Number removed = list.removeAtIndex(0);
    assertEquals(5, removed);
    assertEquals(10, list.getByIndex(0));
    assertEquals(15, list.getByIndex(1));
    list.removeAtIndex(1);
    assertEquals(20, list.getByIndex(1));
  }

  @Test
  void testGet() {
    assertEquals(5, list.get(5));
    assertEquals(10, list.get(10));
    list.removeAtIndex(0);
    list.removeAtIndex(1);
    assertEquals(10, list.get(10));
    assertEquals(20, list.get(20));
  }

  @Test
  void testUpdate() {
    Number updated = list.update(5, 100);
    assertEquals(updated, 100);
    assertEquals(100, list.getByIndex(0));
    list.update(10, 800);
    assertEquals(800, list.getByIndex(1));
  }

  @Test
  void testRemove() {
    assertEquals(5, list.getByIndex(0));
    Number removed = list.remove(5);
    assertEquals(5, removed);
    assertEquals(10, list.getByIndex(0));
    assertEquals(15, list.getByIndex(1));
    list.remove(15);
    assertEquals(20, list.getByIndex(1));
  }

  @Test
  void testToString() {
    assertEquals("""
            5
            10
            15
            20
            """, list.toString());
    list.removeAtIndex(0);
    assertEquals("""
            10
            15
            20
            """, list.toString());
  }

  @Test
  void testEquals() {
    List<Number> list2 = new List<>();
    list2.add(5);
    list2.add(10);
    list2.add(15);
    list2.add(20);
    assertEquals(list, list2);
    list2.removeAtIndex(0);
    assertNotEquals(list, list2);
  }
}

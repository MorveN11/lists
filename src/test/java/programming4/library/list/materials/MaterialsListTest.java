package programming4.library.list.materials;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.Magazine;
import programming4.library.classes.materials.abstractions.Material;

class MaterialsListTest {

  private MaterialsList<Material> materials;

  @BeforeEach
  void setUp() {
    Material material = new Book("bd3ce7a4-1410-451b-8fb9-aeb9598ab322",
            "El nombre del viento", "Patrick Rothfuss", "Fantasía",
            "978-8401352836", 2007, 672, true,
            5, "Book", "Plaza & Janés");
    Material material2 = new Book("68236103-8985-4384-a910-080fb8a521dc",
            "1984", "George Orwell", "Ciencia Ficción",
            "978-0451524935", 1949, 328, true,
            10, "Book", "Signet Classics");
    Material material3 = new Book("ab23e81c-8fa6-40de-a19f-5831882f32e4",
            "El principito", "Antoine de Saint-Exupéry", "Fábula",
            "978-0156013925", 1943, 96, true,
            15, "Book", "Harcourt, Brace & World");
    Material material4 = new Book("b9a458a2-a329-44c0-8bbb-d10fb4d37941",
            "Cien años de soledad", "Gabriel García Márquez", "Realismo mágico",
            "978-0307474728", 1967, 417, true,
            20, "Book", "Vintage Espanol");
    Material material5 = new Magazine("f0c74ad3-ea61-4fa9-bca8-78a3913a0a71",
            "National Geographic", "National Geographic Society", "Ciencia y Naturaleza",
            "978-8482986425", 2021, 148, true,
            100, "Magazine");
    Material material6 = new Magazine("0da5d3cb-419e-46b9-adba-53b8e414b19b",
            "Time", "Time Magazine", "Actualidad",
            "978-1603209796", 2022, 96, true,
            200, "Magazine");
    Material material7 = new Magazine("c286d43c-6d1a-4b32-9425-53327e74dd34",
            "Scientific American", "Springer Nature", "Ciencia",
            "978-0470712588", 2023, 80, true,
            300, "Magazine");
    Material material8 = new Magazine("8663bbc0-eb3c-44f4-a4c3-2e3a95e49608",
            "The New Yorker", "Condé Nast", "Cultura",
            "978-0000000000", 2023, 120, true,
            50, "Magazine");
    Material material9 = new Magazine("d1774e9f-8636-422b-bd75-afbbad7ce7d3",
            "The New Yorker", "Anónimo", "Cultura",
            "978-0000000000", 2023, 120, true,
            50, "Magazine");
    materials = new MaterialsList<>();
    materials.add(material);
    materials.add(material2);
    materials.add(material3);
    materials.add(material4);
    materials.add(material5);
    materials.add(material6);
    materials.add(material7);
    materials.add(material8);
    materials.add(material9);
  }

  @Test
  void filterByTitle() {
    MaterialsList<Material> filteredMaterials = materials.filterByTitle("The New Yorker");
    assertNotNull(filteredMaterials);
    assertEquals(2, filteredMaterials.size());
  }

  @Test
  void filterByAuthor() {
    MaterialsList<Material> filteredMaterials = materials.filterByAuthor("George Orwell");
    assertNotNull(filteredMaterials);
    assertEquals(1, filteredMaterials.size());
  }

  @Test
  void filterByCategory() {
    MaterialsList<Material> filteredMaterials = materials.filterByCategory("Ciencia Ficción");
    assertNotNull(filteredMaterials);
    assertEquals(1, filteredMaterials.size());
  }

  @Test
  void filterByIsbn() {
    MaterialsList<Material> filteredMaterials = materials.filterByIsbn("978-8401352836");
    assertNotNull(filteredMaterials);
    assertEquals(1, filteredMaterials.size());
  }

  @Test
  void filterByPublicationYear() {
    MaterialsList<Material> filteredMaterials = materials.filterByPublicationYear(2023);
    assertNotNull(filteredMaterials);
    assertEquals(3, filteredMaterials.size());
  }

  @Test
  void filterByPageCount() {
    MaterialsList<Material> filteredMaterials = materials.filterByPageCount(120);
    assertNotNull(filteredMaterials);
    assertEquals(2, filteredMaterials.size());
  }

  @Test
  void filterByAvailable() {
    MaterialsList<Material> filteredMaterials = materials.filterByAvailable(true);
    assertNotNull(filteredMaterials);
    assertEquals(9, filteredMaterials.size());
  }

  @Test
  void filterByQuantity() {
    MaterialsList<Material> filteredMaterials = materials.filterByQuantity(50);
    assertNotNull(filteredMaterials);
    assertEquals(2, filteredMaterials.size());
  }

  @Test
  void filterByTypeMaterial() {
    MaterialsList<Material> filteredMaterials = materials.filterByTypeMaterial("Book");
    assertNotNull(filteredMaterials);
    assertEquals(4, filteredMaterials.size());
  }

  @Test
  void filterByEditorial() {
    MaterialsList<Book> filteredMaterials = materials.filterByEditorial("Harcourt, Brace & World");
    assertNotNull(filteredMaterials);
    assertEquals(1, filteredMaterials.size());
  }

  @Test
  void filterOnlyBooks() {
    MaterialsList<Book> filteredBooks = materials.filterOnlyBooks();
    assertNotNull(filteredBooks);
    assertEquals(4, filteredBooks.size());
  }

  @Test
  void filterOnlyMagazines() {
    MaterialsList<Magazine> filteredMagazines = materials.filterOnlyMagazines();
    assertNotNull(filteredMagazines);
    assertEquals(5, filteredMagazines.size());
  }
}

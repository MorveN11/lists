package programming4.library.classes.loans;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.materials.Book;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.utils.Ui;

public class LoanTest {

  private Loan loan;

  @BeforeEach
  void setUp() {
    Person student = new Student("1f6fd5a4-90c7-4bf3-b26d-eeb3dcbcaf84",
            "Santi", "Quiroga", "Av.Melchor Perez de Olguin",
            "santiago.quiroga@gmail.com", 73456209);
    Material book = new Book("bd3ce7a4-1410-451b-8fb9-aeb9598ab322",
            "El nombre del viento", "Patrick Rothfuss", "Fantasía",
            "978-8401352836", 2007, 672, true,
            5, "Plaza & Janés", "Book");
    LocalDate loanDate = LocalDate.of(2023, 7, 15);
    LocalDate returnDate = LocalDate.of(2023, 8, 15);
    loan = new Loan(book, student, loanDate, returnDate);
  }

  @Test
  void testLoanConstructor() {
    Person person = new Teacher("e", "f", "g", "h", 5555555);
    Material material = new Book("El nombre del viento", "Patrick Rothfuss", "Fantasía",
            "978-8401352836", 2007, 672, true, 5, "Plaza & Janés", "Book");
    loan = new Loan(material, person);
    assertNotNull(loan.getLoanDate());
    assertNotNull(loan.getReturnDate());
  }

  @Test
  void testGetters() {
    assertNotNull(loan.getMaterial());
    assertNotNull(loan.getPerson());
    assertEquals(LocalDate.of(2023, 7, 15), loan.getLoanDate());
    assertEquals(LocalDate.of(2023, 8, 15), loan.getReturnDate());
  }

  @Test
  void testSetters() {
    LocalDate newLoanDate = LocalDate.of(2023, 8, 1);
    LocalDate newReturnDate = LocalDate.of(2023, 9, 1);
    loan.setLoanDate(newLoanDate);
    loan.setReturnDate(newReturnDate);
    assertEquals(newLoanDate, loan.getLoanDate());
    assertEquals(newReturnDate, loan.getReturnDate());
  }

  @Test
  void testToString() {
    String expectedString = String.format("""
                    %s
                    Loan ID:       %s
                    Material:      %s - %s
                    Person:        %s %s
                    Loan Date :    %s
                    Return Date:   %s
                    %s
                    """, Ui.printTitle("Loan"), loan.getId(), loan.getMaterial().getTitle(),
            loan.getMaterial().getAuthor(), loan.getPerson().getName(), loan.getPerson().getLastname(),
            loan.getLoanDate(), loan.getReturnDate(), Ui.printLine());
    assertEquals(expectedString, loan.toString());
  }
}

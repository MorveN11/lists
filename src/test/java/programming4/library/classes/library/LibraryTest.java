package programming4.library.classes.library;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.loans.Loan;
import programming4.library.classes.materials.Book;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.treeList.loans.LoansTreeList;
import programming4.library.treeList.materials.MaterialsTreeList;
import programming4.library.treeList.persons.PersonsTreeList;

class LibraryTest {

  private Library library;

  private PersonsTreeList persons;

  private MaterialsTreeList materials;

  private LoansTreeList loans;

  @BeforeEach
  void setUp() {
    library = new Library();
    persons = new PersonsTreeList();
    materials = new MaterialsTreeList();
    loans = new LoansTreeList();
    persons.add(new Student("e", "f", "g", "h", 5555555));
    persons.add(new Teacher("e", "f", "g", "h", 5555555));
    materials.add(new Book("El nombre del viento", "Patrick Rothfuss", "Fantasía",
            "978-8401352836", 2007, 672, true, 5, "Plaza & Janés", "Book"));
    materials.add(new Book("1984", "George Orwell", "Ciencia Ficción",
            "978-0451524935", 1949, 328, true, 10, "Signet Classics", "Book"));
    loans.add(new Loan(materials.getList().getByIndex(0), persons.getList().getByIndex(0)));
    loans.add(new Loan(materials.getList().getByIndex(0), persons.getList().getByIndex(1)));
    library.loadPersons(persons);
    library.loadMaterials(materials);
    library.loadLoans(loans);
  }

  @Test
  void getPersons() {
    assertEquals(persons, library.getPersons());
  }

  @Test
  void getMaterials() {
    assertEquals(materials, library.getMaterials());
  }

  @Test
  void getLoans() {
    assertEquals(loans, library.getLoans());
  }
}

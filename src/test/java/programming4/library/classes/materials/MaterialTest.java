package programming4.library.classes.materials;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.materials.abstractions.Material;
import programming4.library.errors.NotImplementedFunctionException;

class MaterialTest {

  private Material material;

  @BeforeEach
  void setUp() {
    material = new Material("Título del libro", "Autor del libro",
            "Categoría del libro", "ISBN del libro", 2023,
            300, true, 1, "Libro");
  }

  @Test
  void testGetters() {
    assertNotNull(material.getId());
    assertEquals("Título del libro", material.getTitle());
    assertEquals("Autor del libro", material.getAuthor());
    assertEquals("Categoría del libro", material.getCategory());
    assertEquals("ISBN del libro", material.getIsbn());
    assertEquals(2023, material.getPublicationYear());
    assertEquals(300, material.getPageCount());
    assertTrue(material.isAvailable());
    assertEquals(1, material.getQuantity());
    assertEquals("Libro", material.getTypeMaterial());
  }

  @Test
  void testSetters() {
    material.setTitle("Título del libro 2");
    material.setAuthor("Autor del libro 2");
    material.setCategory("Categoría del libro 2");
    material.setIsbn("ISBN del libro 2");
    material.setPublicationYear(2024);
    material.setPageCount(400);
    material.setAvailable(false);
    material.setQuantity(2);
    material.setTypeMaterial("Libro 2");
    assertEquals("Título del libro 2", material.getTitle());
    assertEquals("Autor del libro 2", material.getAuthor());
    assertEquals("Categoría del libro 2", material.getCategory());
    assertEquals("ISBN del libro 2", material.getIsbn());
    assertEquals(2024, material.getPublicationYear());
    assertEquals(400, material.getPageCount());
    assertFalse(material.isAvailable());
    assertEquals(2, material.getQuantity());
    assertEquals("Libro 2", material.getTypeMaterial());
  }

  @Test
  void testToString() {
    String expectedString = """
            ID:               %s
            Title:            %s
            Author:           %s
            Category:         %s
            ISBN:             %s
            Publication Year: %d
            Page Count:       %d
            Available:        %b
            Quantity:         %d
            Type Material:    %s""".formatted(material.getId().toString(), material.getTitle(), material.getAuthor(),
            material.getCategory(), material.getIsbn(), material.getPublicationYear(),
            material.getPageCount(), material.isAvailable(), material.getQuantity(),
            material.getTypeMaterial());
    assertEquals(expectedString, material.toString());
  }

  @Test
  void testHighMaterial() {
    assertThrows(NotImplementedFunctionException.class, () -> material.highMaterial());
  }

  @Test
  void testLowMaterial() {
    assertThrows(NotImplementedFunctionException.class, () -> material.lowMaterial());
  }

  @Test
  void testChangeMaterial() {
    assertThrows(NotImplementedFunctionException.class, () -> material.changeMaterial());
  }
}

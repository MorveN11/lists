package programming4.library.classes.materials;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.utils.Ui;

class MagazineTest {

  Magazine magazine;

  @BeforeEach
  void setUp() {
    magazine = new Magazine("Título del libro", "Autor del libro",
            "Categoría del libro", "ISBN del libro", 2023,
            300, true, 1, "Revista");
  }

  @Test
  void testToString() {
    String expectedString = """
            %s
            ID:               %s
            Title:            %s
            Author:           %s
            Category:         %s
            ISBN:             %s
            Publication Year: %d
            Page Count:       %d
            Available:        %b
            Quantity:         %d
            Type Material:    %s
            %s
            """.formatted(Ui.printTitle("Magazine"), magazine.getId().toString(), magazine.getTitle(), magazine.getAuthor(),
            magazine.getCategory(), magazine.getIsbn(), magazine.getPublicationYear(),
            magazine.getPageCount(), magazine.isAvailable(), magazine.getQuantity(),
            magazine.getTypeMaterial(), Ui.printLine());
    assertEquals(expectedString, magazine.toString());
  }
}

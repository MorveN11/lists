package programming4.library.classes.materials;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.utils.Ui;

class BookTest {

  private Book book;

  @BeforeEach
  void setUp() {
    book = new Book("Título del libro", "Autor del libro",
            "Categoría del libro", "ISBN del libro", 2023,
            300, true, 1, "Libro", "Editorial del libro");
  }

  @Test
  void testGetters() {
    assertEquals("Editorial del libro", book.getEditorial());
  }

  @Test
  void testSetters() {
    book.setEditorial("Editorial del libro 2");
    assertEquals("Editorial del libro 2", book.getEditorial());
  }

  @Test
  void testToString() {
    String expectedString = """
            %s
            ID:               %s
            Title:            %s
            Author:           %s
            Category:         %s
            ISBN:             %s
            Publication Year: %d
            Page Count:       %d
            Available:        %b
            Quantity:         %d
            Type Material:    %s
            Editorial:        %s
            %s
            """.formatted(Ui.printTitle("Book"), book.getId().toString(), book.getTitle(), book.getAuthor(),
            book.getCategory(), book.getIsbn(), book.getPublicationYear(),
            book.getPageCount(), book.isAvailable(), book.getQuantity(),
            book.getTypeMaterial(), book.getEditorial(), Ui.printLine());
    assertEquals(expectedString, book.toString());
  }
}

package programming4.library.classes.persons;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.utils.Ui;

class TeacherTest {

  private Teacher teacher;

  @BeforeEach
  void setUp() {
    teacher = new Teacher("John", "Doe", "123 Main St",
            "john.doe@example.com", 123456789);
  }

  @Test
  void testToString() {
    String expectedString = String.format("""
                    %s
                    ID:               %s
                    Name:             %s
                    Lastname:         %s
                    Address:          %s
                    Email:            %s
                    Phone Number:     %s
                    Number of books:  %d
                    Debt:             %f
                    %s
                    """, Ui.printTitle("Teacher"),
            teacher.getId().toString(), teacher.getName(), teacher.getLastname(), teacher.getAddress(),
            teacher.getEmail(), teacher.getPhoneNumber(), teacher.getNumBooks(), teacher.getDebt(), Ui.printLine());
    assertEquals(expectedString, teacher.toString());
  }
}

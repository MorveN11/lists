package programming4.library.classes.persons;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.errors.NotMoreMaterialsToReturnException;

public class PersonTest {

  private Person person;

  @BeforeEach
  void setUp() {
    person = new Person("John", "Doe", "123 Main St",
            "john.doe@example.com", 123456789);
  }

  @Test
  void testGetters() {
    assertNotNull(person.getId().toString());
    assertEquals("John", person.getName());
    assertEquals("Doe", person.getLastname());
    assertEquals("123 Main St", person.getAddress());
    assertEquals("john.doe@example.com", person.getEmail());
    assertEquals(123456789, person.getPhoneNumber());
    assertEquals(0, person.getNumBooks());
    assertEquals(0, person.getDebt(), 0.01);
  }

  @Test
  void testSetters() {
    person.setNumBooks(10);
    person.setPhoneNumber(987654321);
    person.setDebt(20.75);
    person.setName("Jane");
    person.setLastname("Smith");
    person.setEmail("jane.smith@example.com");
    person.setAddress("456 Oak St");
    assertEquals(10, person.getNumBooks());
    assertEquals(987654321, person.getPhoneNumber());
    assertEquals(20.75, person.getDebt(), 0.01);
    assertEquals("Jane", person.getName());
    assertEquals("Smith", person.getLastname());
    assertEquals("jane.smith@example.com", person.getEmail());
    assertEquals("456 Oak St", person.getAddress());
  }

  @Test
  void testToString() {
    String expectedString = String.format("""
                    ID:               %s
                    Name:             %s
                    Lastname:         %s
                    Address:          %s
                    Email:            %s
                    Phone Number:     %s
                    Number of books:  %d
                    Debt:             %f""",
            person.getId().toString(), person.getName(), person.getLastname(), person.getAddress(),
            person.getEmail(), person.getPhoneNumber(), person.getNumBooks(), person.getDebt());
    assertEquals(expectedString, person.toString());
  }

  @Test
  void testTakeMaterial() {
    person.takeMaterial();
    assertEquals(1, person.getNumBooks());
  }

  @Test
  void testReturnMaterial() {
    assertThrows(NotMoreMaterialsToReturnException.class, () -> person.returnMaterial());
    person.takeMaterial();
    person.returnMaterial();
    assertEquals(0, person.getNumBooks());
  }
}

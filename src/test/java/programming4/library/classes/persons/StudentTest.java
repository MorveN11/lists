package programming4.library.classes.persons;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.utils.Ui;

class StudentTest {

  private Student student;

  @BeforeEach
  void setUp() {
    student = new Student("John", "Doe", "123 Main St",
            "john.doe@example.com", 123456789);
  }

  @Test
  void testToString() {
    String expectedString = String.format("""
                    %s
                    ID:               %s
                    Name:             %s
                    Lastname:         %s
                    Address:          %s
                    Email:            %s
                    Phone Number:     %s
                    Number of books:  %d
                    Debt:             %f
                    %s
                    """, Ui.printTitle("Student"),
            student.getId().toString(), student.getName(), student.getLastname(), student.getAddress(),
            student.getEmail(), student.getPhoneNumber(), student.getNumBooks(), student.getDebt(), Ui.printLine());
    assertEquals(expectedString, student.toString());
  }
}

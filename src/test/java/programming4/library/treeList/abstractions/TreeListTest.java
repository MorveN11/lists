package programming4.library.treeList.abstractions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TreeListTest {

  private TreeList<String> treeList;

  @BeforeEach
  void setUp() {
    treeList = new TreeList<>();
    treeList.add("apple");
    treeList.add("banana");
    treeList.add("cherry");
    treeList.add("date");
  }

  @Test
  void testAdd() {
    treeList.add("grape");
    assertEquals("apple banana cherry date grape", treeList.getTree().inOrder());
    assertNull(treeList.add("banana"));
    assertEquals("apple banana cherry date grape", treeList.getTree().inOrder());
  }

  @Test
  void testRemove() {
    assertEquals("apple banana cherry date", treeList.getTree().inOrder());
    treeList.remove("cherry");
    assertEquals("apple banana date", treeList.getTree().inOrder());
  }

  @Test
  void testGet() {
    assertEquals("banana", treeList.getByTree("banana"));
    assertEquals("banana", treeList.getByList("banana"));
  }

  @Test
  void testUpdate() {
    String updatedValue = treeList.update("banana", "mango");
    assertEquals("mango", updatedValue);
    assertEquals("apple mango cherry date", treeList.getTree().inOrder());
  }
}

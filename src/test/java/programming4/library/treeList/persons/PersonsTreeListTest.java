package programming4.library.treeList.persons;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;
import programming4.library.list.persons.PersonsList;

class PersonsTreeListTest {

  private PersonsTreeList persons;

  @BeforeEach
  void setUp() {
    Person person = new Student("05a8def5-2380-47a5-849a-06fe260ded95",
            "Jose", "Morales", "Av. Blanco Galindo",
            "morales.patty.jose@gmail.com", 75463290);
    Person person2 = new Teacher("3d686983-ec44-42e1-8eea-e637ebc3f034",
            "Miguel", "Romero", "Av. America",
            "micky.romesa@gmail.com", 75423890);
    Person person3 = new Student("bf9818ea-8eb9-4c19-aa4f-118f54391e39",
            "Maria", "González", "Calle 123",
            "juan.lopez@example.com", 12345678);
    Person person4 = new Teacher("8d8f64fa-f430-4890-92db-d0f8b89a8c64",
            "Miguel", "Martínez", "Avenida XYZ",
            "luis.martinez@example.com", 87654321);
    Person person5 = new Student("8f9eb24a-c116-4f84-8987-7dd3d31bd739",
            "Ana", "Pérez", "Carrera 123",
            "ana.perez@example.com", 56781234);
    Person person6 = new Teacher("c26e5d9a-0cbd-4f3a-bb7b-c12d379d18da",
            "Juan", "López", "Calle 789",
            "juan.lopez@example.com", 43218765);
    Person person7 = new Student("6b381ac9-3217-4c98-bb3f-ac673d5f090c",
            "Carla", "Pérez", "Avenida ABC",
            "carla.ramirez@example.com", 75463290);
    persons = new PersonsTreeList();
    persons.add(person);
    persons.add(person2);
    persons.add(person3);
    persons.add(person4);
    persons.add(person5);
    persons.add(person6);
    persons.add(person7);
  }

  @Test
  void filterByName() {
    PersonsList<Person> filteredPersons = persons.filterByName("Miguel");
    assertNotNull(filteredPersons);
    assertEquals(2, filteredPersons.size());
  }

  @Test
  void filterByLastname() {
    PersonsList<Person> filteredPersons = persons.filterByLastname("Pérez");
    assertNotNull(filteredPersons);
    assertEquals(2, filteredPersons.size());
  }

  @Test
  void filterByAddress() {
    PersonsList<Person> filteredPersons = persons.filterByAddress("Calle 123");
    assertNotNull(filteredPersons);
    assertEquals(2, filteredPersons.size());
  }

  @Test
  void filterByEmail() {
    PersonsList<Person> filteredPersons = persons.filterByEmail("juan.lopez@example.com");
    assertNotNull(filteredPersons);
    assertEquals(5, filteredPersons.size());
  }

  @Test
  void filterByPhoneNumber() {
    PersonsList<Person> filteredPersons = persons.filterByPhoneNumber(75463290);
    assertNotNull(filteredPersons);
    assertEquals(2, filteredPersons.size());
  }

  @Test
  void filterByNumBooks() {
    PersonsList<Person> filteredPersons = persons.filterByNumBooks(0);
    assertNotNull(filteredPersons);
    assertEquals(7, filteredPersons.size());
  }

  @Test
  void filterByDebt() {
    PersonsList<Person> filteredPersons = persons.filterByDebt(0);
    assertNotNull(filteredPersons);
    assertEquals(7, filteredPersons.size());
  }

  @Test
  void filterOnlyStudents() {
    PersonsList<Student> filteredStudents = persons.filterOnlyStudents();
    assertNotNull(filteredStudents);
    assertEquals(4, filteredStudents.size());
  }

  @Test
  void filterOnlyTeachers() {
    PersonsList<Teacher> filteredTeachers = persons.filterOnlyTeachers();
    assertNotNull(filteredTeachers);
    assertEquals(3, filteredTeachers.size());
  }
}

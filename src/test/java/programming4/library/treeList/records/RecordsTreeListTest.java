package programming4.library.treeList.records;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import programming4.library.classes.persons.Student;
import programming4.library.classes.persons.Teacher;
import programming4.library.classes.persons.abstractions.Person;

class RecordsTreeListTest {

  private RecordsTreeList<Person> persons;

  @BeforeEach
  void setUp() {
    persons = new RecordsTreeList<>();
    persons.add(new Student("e", "f", "g", "h", 5555555));
    persons.add(new Teacher("e", "f", "g", "h", 5555555));
    persons.add(new Student("e", "f", "g", "h", 5555555));
    persons.add(new Teacher("e", "f", "g", "h", 5555555));
    persons.add(new Student("e", "f", "g", "h", 5555555));
    persons.add(new Teacher("e", "f", "g", "h", 5555555));
  }

  @Test
  void getById() {
    Person person = persons.getList().getByIndex(0);
    Person equalPerson = persons.getByIdByTree(person.getId());
    Person equalStringPerson = persons.getByIdByTree(person.getId().toString());
    assertEquals(person, equalPerson);
    assertEquals(person, equalStringPerson);
    Person person2 = persons.getList().getByIndex(0);
    Person equalPerson2 = persons.getByIdByList(person.getId());
    Person equalStringPerson2 = persons.getByIdByList(person.getId().toString());
    assertEquals(person2, equalPerson2);
    assertEquals(person2, equalStringPerson2);
  }

  @Test
  void updateById() {
    Person person1 = persons.getList().getByIndex(0);
    Person person1Update = new Student("e", "f", "g", "h", 5555555);
    assertNotNull(persons.updateById(person1.getId(), person1Update));
    assertEquals(person1Update, persons.getList().getByIndex(0));
  }

  @Test
  void removeById() {
    Person person1 = persons.getList().getByIndex(0);
    Person person2 = persons.getList().getByIndex(1);
    Person person3 = persons.getList().getByIndex(2);
    assertNotNull(persons.removeById(person2.getId().toString()));
    assertNotNull(persons.removeById(person1.getId()));
    assertEquals(person3, persons.getList().getByIndex(0));
  }
}

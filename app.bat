@echo off

where java >nul 2>&1
if %errorlevel% equ 0 (
    echo Java está instalado en la máquina.
    echo "Iniciando Aplicación"
    ./gradlew build
    ./gradlew assemble
     java -jar ./build/libs/library-1.0.jar
) else (
    echo Java no está instalado en la máquina.
    echo "Terminando Proceso"
)
